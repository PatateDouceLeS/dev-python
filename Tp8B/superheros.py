"""
TP8B
"""

def intelligence_moyenne(super_dico):
    """Donne l'intelligence moyenne des super-héros

    Args:
        super_dico (dict): Dictionnaire contenant les caractérique des super-héros

    Returns:
        float: Moyenne de l'intelligence des super_héros
    """
    cpt = 0
    total = 0
    for (_, intel, _) in super_dico.values():
        total += intel
        cpt += 1
    if cpt == 0:
        return None
    return total/cpt

def kikelplusfort(super_dico):
    """Renvoie le nom du plus fort parmi parmi les super-héros du dictionnaire

    Args:
        super_dico (dict): Dictionnaire contenant les caractérique des super-héros

    Returns:
        str: Nom du plus fort
    """
    plus_fort = None
    force_max = None
    for (nom_heros, (force, _, _)) in super_dico.items():
        if force_max == None or force > force_max:
            force_max = force
            plus_fort = nom_heros
    return plus_fort

def combienDeCretins(super_dico):
    """Renvoie le nombre de héros dont l'intelligence est inférieur à la moyenne

    Args:
        super_dico (dict): Dictionnaire contenant les caractérique des super-héros

    Returns:
        int: Le nombre de héros dont l'intelligence est inférieur à la moyenne
    """
    nb_débiles = 0
    moy_intel = intelligence_moyenne(super_dico)
    for (_, (_, intel, _)) in super_dico.items():
        if intel < moy_intel:
            nb_débiles += 1
    return nb_débiles