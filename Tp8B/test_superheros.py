import superheros as super

AVENGERS = {'Spiderman':(5, 5, 'araignée a quatre pattes'),
        'Hulk': (7, 4, "Grand homme vert"),
        'Agent 13': (2, 3, "agent 13"),
        'M Becker': (2, 6, 'expert en graphe')
        }
EXEMPLE1 = {'a':(1, 1, 'a'), 'b':(3, 9, 'b'), 'c':(7, 2, 'c')}
EXEMPLE2 = {'a':(1, 1, 'a'), 'b':(3, 9, 'b'), 'd':(4, 4, 'd')}



def test_intelligence_moyenne():
    assert super.intelligence_moyenne({}) is None
    assert super.intelligence_moyenne(EXEMPLE1) == 4
    assert abs(super.intelligence_moyenne(EXEMPLE2)-14/3) <= 0.01
    assert super.intelligence_moyenne(AVENGERS) == 4.5

def test_kikelplusfort():
    assert super.kikelplusfort({}) is None
    assert super.kikelplusfort(EXEMPLE1) == 'c'
    assert super.kikelplusfort(EXEMPLE2) == 'd'
    assert super.kikelplusfort(AVENGERS) == 'Hulk'

def test_combienDeCretins():
    assert super.combienDeCretins({}) == 0
    assert super.combienDeCretins(EXEMPLE1) == 2
    assert super.combienDeCretins(EXEMPLE2) == 2
    assert super.combienDeCretins(AVENGERS) == 2