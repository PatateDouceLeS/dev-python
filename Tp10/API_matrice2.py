""" Matrices : API n 2 """

def construit_matrice(nb_lignes , nb_colonnes , valeur_par_defaut =0):
    """crée une nouvelle matrice en mettant la valeur par défaut dans chacune de ses cases.

    Args:
        nb_lignes (int): le nombre de lignes de la matrice
        nb_colonnes (int): le nombre de colonnes de la matrice
        valeur_par_defaut : La valeur que prendra chacun des éléments de la matrice

    Returns:
        une nouvelle matrice qui contient la valeur par défaut dans chacune de ses cases
    """
    matrice_def = []
    ligne_cour = []
    for _ in range(nb_lignes):
        for _ in range(nb_colonnes):
            ligne_cour.append(valeur_par_defaut)
        matrice_def.append(ligne_cour)
        ligne_cour = []
    return matrice_def
 
def get_nb_lignes(matrice):
    """permet de connaître le nombre de lignes d'une matrice

    Args:
        matrice : une matrice

    Returns:
        int : le nombre de lignes de la matrice
    """
    return len(matrice)

def get_nb_colonnes(matrice):
    """permet de connaître le nombre de colonnes d'une matrice

    Args:
        matrice : une matrice

    Returns:
        int : le nombre de colonnes de la matrice
    """
    return len(matrice[0])

def get_val(matrice , ligne , colonne):
    """permet de connaître la valeur de l'élément de la matrice dont on connaît
    le numéro de ligne et le numéro de colonne.

    Args:
        matrice : une matrice
        ligne (int) : le numéro d'une ligne (la numérotation commence à zéro)
        colonne (int) : le numéro d'une colonne (la numérotation commence à zéro)

    Returns:
        la valeur qui est dans la case située à la ligne et la colonne spécifiées
    """
    return matrice[ligne][colonne]

def set_val(matrice , ligne , colonne , nouvelle_valeur):
    """permet de modifier la valeur de l'élément qui se trouve à la ligne et à la colonne
    spécifiées. Cet élément prend alors la valeur nouvelle_valeur

    Args:
        matrice : une matrice
        ligne (int) : le numéro d'une ligne (la numérotation commence à zéro)
        colonne (int) : le numéro d'une colonne (la numérotation commence à zéro)
        nouvelle_valeur : la nouvelle valeur que l'on veut mettre dans la case

    Returns:
        None
    """
    matrice[ligne][colonne] = nouvelle_valeur

def get_ligne(matrice , ligne):
    """Renvoie la ligne donné par son numéro

    Args:
        matrice (tuple): Une matrice

    Returns:
        list: Les valeurs de la ligne demandée
    """
    return matrice[ligne]

def get_colonne(matrice , colonne):
    """Renvoie la colonne donné par son numéro

    Args:
        matrice (tuple): Une matrice

    Returns:
        list: Les valeurs de la colonne demandée
    """
    val_colonne = []
    for ligne in matrice:
        val_colonne.append(ligne[colonne])
    return val_colonne

