"""
Tests pour les API matrices
Remarques : tous les tests de ce fichier doivent passer quelle que soit l'API utilisée
"""
import API_matrice2 as API
import utilitaires_matrice as util

def matrice1():
    m1 = API.construit_matrice(3, 4, None)
    API.set_val(m1, 0, 0, 10)
    API.set_val(m1, 0, 1, 11)    
    API.set_val(m1, 0, 2, 12)
    API.set_val(m1, 0, 3, 13)
    API.set_val(m1, 1, 0, 14)
    API.set_val(m1, 1, 1, 15)
    API.set_val(m1, 1, 2, 16)
    API.set_val(m1, 1, 3, 17)
    API.set_val(m1, 2, 0, 18)
    API.set_val(m1, 2, 1, 19)
    API.set_val(m1, 2, 2, 20)
    API.set_val(m1, 2, 3, 21)
    return m1

def matrice2():
    m2 = API.construit_matrice(2, 3, None)
    API.set_val(m2, 0, 0, 'A')
    API.set_val(m2, 0, 1, 'B')
    API.set_val(m2, 0, 2, 'C')
    API.set_val(m2, 1, 0, 'D')
    API.set_val(m2, 1, 1, 'E')
    API.set_val(m2, 1, 2, 'F')
    return m2

def matrice3():
    m3 = API.construit_matrice(3, 3, None)
    API.set_val(m3, 0, 0, 2)
    API.set_val(m3, 0, 1, 7)
    API.set_val(m3, 0, 2, 6)
    API.set_val(m3, 1, 0, 9)
    API.set_val(m3, 1, 1, 5)
    API.set_val(m3, 1, 2, 1)
    API.set_val(m3, 2, 0, 4)
    API.set_val(m3, 2, 1, 3)
    API.set_val(m3, 2, 2, 8)
    return m3

M1 = matrice1()
M2 = matrice2()
M3 = matrice3()


def test_get_nb_lignes():
    assert API.get_nb_lignes(M1) == 3
    assert API.get_nb_lignes(M2) == 2
    assert API.get_nb_lignes(M3) == 3
    
def test_get_nb_colonnes():
    assert API.get_nb_colonnes(M1) == 4
    assert API.get_nb_colonnes(M2) == 3
    assert API.get_nb_colonnes(M3) == 3

def test_get_val():
    assert API.get_val(M1, 0, 1) == 11
    assert API.get_val(M1, 2, 1) == 19
    assert API.get_val(M2, 1, 1) == 'E'
    assert API.get_val(M2, 0, 2) == 'C'
    assert API.get_val(M3, 2, 0) == 4
    assert API.get_val(M3, 1, 0) == 9

def test_get_ligne():
    assert API.get_ligne(M1, 0) == [10, 11, 12, 13]
    assert API.get_ligne(M2, 1) == ['D', 'E', 'F']
    assert API.get_ligne(M3, 2) == [4, 3, 8]


def get_colonne():
    assert API.get_colonne(M1, 0) == [10, 14, 18]
    assert API.get_colonne(M2, 1) == ['B', 'E']
    assert API.get_colonne(M3, 2) == [6, 1, 8]

"""
def test_sauve_charge_matrice():
    matrice = matrice2()
    API.sauve_matrice(matrice, "matrice.csv")
    matrice_bis = API.charge_matrice_str("matrice.csv")
    assert matrice == matrice_bis
"""

def test_get_diagonale_principale():
    assert util.get_diagonale_principale(M1) == "Il faut une matrice carré"
    assert util.get_diagonale_principale(M2) == "Il faut une matrice carré"
    assert util.get_diagonale_principale(M3) == [2, 5, 8]


def test_get_diagonale_secondaire():
    assert util.get_diagonale_secondaire(M1) == "Il faut une matrice carré"
    assert util.get_diagonale_secondaire(M2) == "Il faut une matrice carré"
    assert util.get_diagonale_secondaire(M3) == [4, 5, 6]

def test_transposee():
    assert util.transposee(M1) == "Il faut une matrice carré"
    assert util.transposee(M2) == "Il faut une matrice carré"
    assert util.transposee(M3) == [[2, 9, 4], [7, 5, 3], [6, 1, 8]]

def test_is_triangulaire_inf():
    assert util.is_triangulaire_inf(M1) == "Il faut une matrice carré"
    assert util.is_triangulaire_inf(M2) == "Il faut une matrice carré"
    assert not util.is_triangulaire_inf(M3)
    m4 = [[1,0,0,0], [1,1,0,0], [1,1,1,0], [1,1,1,1]]
    assert util.is_triangulaire_inf(m4)

def test_is_triangulaire_sup():
    assert util.is_triangulaire_sup(M1) == "Il faut une matrice carré"
    assert util.is_triangulaire_sup(M2) == "Il faut une matrice carré"
    assert not util.is_triangulaire_sup(M3)
    m4 = [[1,1,1,1], [0,1,1,1], [0,0,1,1], [0,0,0,1]]
    assert util.is_triangulaire_sup(m4)
