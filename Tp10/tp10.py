"""Init Dev : TP10"""

# =====================================================================
# Exercice 1 : Choix de modélisation et complexité
# =====================================================================
# Modélisation n°1
# =====================================================================

# Penser à completer la fonction exemples_pokedex_v1 dans le fichier de tests

def appartient_v1(pokemon, pokedex): 
    """ renvoie True si pokemon (str) est présent dans le pokedex """
    for (poke, _) in pokedex:
        if poke == pokemon:
            return True
    return False
#Compléxité : O(N)


def toutes_les_attaques_v1(pokemon, pokedex): 
    """
    param: un pokedex et le nom d'un pokemon (str) qui appartient au pokedex
    resultat: renvoie l'ensemble des types d'attaque du pokemon en paramètre
    """
    types = set()
    for (poke, attaque) in pokedex:
        if poke == pokemon:
            types.add(attaque)
    return types
#Compléxité : O(N)


def nombre_de_v1(attaque, pokedex): 
    """
    param: un pokedex et un type d'attaque (str)
    resultat: renvoie le nombre de pokemons de ce type d'attaque
    dans le pokedex
    """
    cpt_att = 0
    for (_, types) in pokedex:
        if attaque == types:
            cpt_att += 1
    return cpt_att
#Compléxité : O(N)


def dico_freq_pokemon_v1(pokedex):
    dico_freq = dict()
    for (_, attaque) in pokedex:
        if attaque not in dico_freq:
            dico_freq[attaque] = 1
        else:
            dico_freq[attaque] += 1
    return dico_freq
#Compléxité : O(N)


def attaque_preferee_v1(pokedex):
    """
    Renvoie le nom du type d'attaque qui est la plus fréquente dans le pokedex
    """
    dico_freq = dico_freq_pokemon_v1(pokedex)
    max_att = None
    att_pref = None
    for (attaque, nbr_att) in dico_freq.items():
        if max_att == None or max_att < nbr_att:
            max_att = nbr_att
            att_pref = attaque
    return att_pref
#Compléxité : O(N)


# =====================================================================
# Modélisation n°2
# =====================================================================

# Penser à completer la fonction exemples_pokedex_v2 dans le fichier de tests

def appartient_v2(pokemon, pokedex):
    """ renvoie True si pokemon (str) est présent dans le pokedex """
    return pokemon in pokedex
#Compléxité : O(1)


def toutes_les_attaques_v2(pokemon, pokedex):
    """
    param: un pokedex et le nom d'un pokemon (str) qui appartient au pokedex
    resultat: renvoie l'ensemble des types d'attaque du pokemon é en paramètre
    """
    ens_att = set()
    for (poke, attaques) in pokedex.items():
        if poke == pokemon:
            for attaque in attaques:
                ens_att.add(attaque)
    return ens_att
#Compléxité : O(N²)


def nombre_de_v2(attaque, pokedex):
    """
    param: un pokedex et un type d'attaque (str)
    resultat: renvoie le nombre de pokemons de ce type d'attaque
    dans le pokedex
    """
    cpt_att = 0
    for attaques in pokedex.values():
        if attaque in attaques:
            cpt_att += 1
    return cpt_att
#Compléxité : O(N)


def dico_freq_pokemon_v2(pokedex):
    dico_freq = dict()
    for attaques in pokedex.values():
        for attaque in attaques:
            if attaque not in dico_freq:
                dico_freq[attaque] = 1
            else:
                dico_freq[attaque] += 1
    return dico_freq
#Compléxité : O(N²)


def attaque_preferee_v2(pokedex):
    """
    Renvoie le nom du type d'attaque qui est la plus fréquente dans le pokedex
    """
    dico_freq = dico_freq_pokemon_v2(pokedex)
    max_att = None
    att_pref = None
    for (attaque, nbr_att) in dico_freq.items():
        if max_att == None or max_att < nbr_att:
            max_att = nbr_att
            att_pref = attaque
    return att_pref
#Compléxité : O(N²)


# =====================================================================
# Modélisation n°3
# =====================================================================

# Penser à completer la fonction exemples_pokedex_v3 dans le fichier de tests


def appartient_v3(pokemon, pokedex):
    """ renvoie True si pokemon (str) est présent dans le pokedex """
    for poke in pokedex.values():
        if pokemon in poke:
            return True
    return False
#Compléxité : O(N)


def toutes_les_attaques_v3(pokemon, pokedex):
    """
    param: un pokedex et le nom d'un pokemon (str) qui appartient au pokedex
    resultat: renvoie l'ensemble des types d'attaque du pokemon en paramètre
    """
    typ_att = set()
    for (famille, poke) in pokedex.items():
        if pokemon in poke:
            typ_att.add(famille)
    return typ_att
#Compléxité : O(N)


def nombre_de_v3(attaque, pokedex):
    """
    param: un pokedex et un type d'attaque (str)
    resultat: renvoie le nombre de pokemons de ce type d'attaque
    dans le pokedex
    """
    for famille in pokedex:
        if famille == attaque:
            return len(pokedex[famille])
    return 0
#Compléxité : O(N)


def attaque_preferee_v3(pokedex):
    """
    Renvoie le nom du type d'attaque qui est la plus fréquente dans le pokedex
    """
    max_att = None
    att_pop = None
    for (famille, poke) in pokedex.items():
        if max_att == None or len(poke) > max_att:
            max_att = len(poke)
            att_pop = famille
    return att_pop
#Compléxité : O(N)


# =====================================================================
# Transformations
# =====================================================================

# Version 1 ==> Version 2

def v1_to_v2(pokedex_v1):
    """
    param: prend en paramètre un pokedex version 1
    renvoie le même pokedex mais en version 2
    """
    poke_v2 = dict()
    for (poke, attaque) in pokedex_v1:
        if poke not in poke_v2:
            poke_v2[poke] = {attaque}
        else:
            poke_v2[poke].add(attaque)
    return poke_v2

pokedex_anakin_v1 = {
    ('Carmache', 'Dragon'), ('Carmache', 'Sol'),
    ('Colimucus', 'Dragon'), ('Palkia', 'Dragon'),
    ('Palkia', 'Eau')}

pokedex_anakin_v2 = {
    'Carmache': {'Dragon','Sol'},
    'Colimucus': {'Dragon'},
    'Palkia': {'Dragon', 'Eau'}}

pokedex_anakin_v3 = {
    'Dragon': {'Carmache','Colimucus', 'Palkia'},
    'Sol': {'Carmache'},
    'Eau': {'Palkia'}}

# Version 2 ==> Version 3

def v2_to_v3(pokedex_v2):
    """
    param: prend en paramètre un pokedex version2
    renvoie le même pokedex mais en version3
    """
    poke_v3 = dict()
    for (poke, attaques) in pokedex_v2.items():
        for att in attaques:
            if att not in poke_v3:
                poke_v3[att] = {poke}
            else:
                poke_v3[att].add(poke)
    return poke_v3


# =====================================================================
# Exercice 2 : Ecosystème
# =====================================================================

#Exercice 2.1
def extinction_immediate(ecosysteme, animal):
    """
    renvoie True si animal s'éteint immédiatement dans l'écosystème faute
    de nourriture
    """
    proie_ani = None
    if animal in ecosysteme:
        proie_ani = ecosysteme[animal]
    return not proie_ani in ecosysteme and proie_ani is not None
        
#Exercice 2.2
def en_voie_disparition(ecosysteme, animal):
    """
    renvoie True si animal s'éteint est voué à disparaitre à long terme
    """
    animal_courant = animal
    voie_ext = False
    cpt = 0
    while cpt <= len(ecosysteme) and (not voie_ext) and (not animal_courant is None):
        if extinction_immediate(ecosysteme, animal_courant):
            voie_ext = True
        else:
            animal_courant = ecosysteme[animal_courant]
        cpt += 1
    return voie_ext 


def animaux_en_danger(ecosysteme):
    """ renvoie l'ensemble des animaux qui sont en danger d'extinction immédiate"""
    pass


def especes_en_voie_disparition(ecosysteme):
    """ renvoie l'ensemble des animaux qui sont en voués à disparaitre à long terme
    """
    pass

# =====================================================================
# Exercice 4 : Petites bêtes
# =====================================================================

#Exercice 4.1:
def pokemons_par_famille(liste_pokemon):
    dico_type = dict()
    for (poke, famille, _) in liste_pokemon:
        for type in famille:
            if type not in dico_type:
                dico_type[type] = {poke}
            else:
                dico_type[type].add(poke)
    return dico_type