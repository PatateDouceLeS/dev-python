""" Fonctions utilitaires pour manipuler les matrices """

import API_matrice2 as mat

def get_diagonale_principale(matrice):
    """Donne la diagonale principale d'une matrice

    Args:
        matrice (list): matrice quelconque

    Returns:
        list: La diagonale principale de la matrice
    """
    if mat.get_nb_lignes(matrice) == mat.get_nb_colonnes(matrice):
        diago_principale = []
        for ind in  range(mat.get_nb_lignes(matrice)):
            diago_principale.append(mat.get_val(matrice, ind, ind))
        return diago_principale
    else:
        return "Il faut une matrice carré"

def get_diagonale_secondaire(matrice):
    """Donne la diagonale secondaire d'une matrice

    Args:
        matrice (list): matrice quelconque

    Returns:
        list: La diagonale secondaire de la matrice
    """
    if mat.get_nb_lignes(matrice) == mat.get_nb_colonnes(matrice):
        diago_secondaire = []
        long = mat.get_nb_lignes(matrice)
        for ind in  range(long):
            diago_secondaire.append(mat.get_val(matrice, long-1-ind, ind))
        return diago_secondaire
    else:
        return "Il faut une matrice carré"

def transposee(matrice):
    """Renvoie la transposée d'une matrice

    Args:
        matrice (list): matrice quelconque

    Returns:
        list: La transposée d'une matrice
    """
    if mat.get_nb_lignes(matrice) == mat.get_nb_colonnes(matrice):
        long = mat.get_nb_colonnes(matrice)
        larg = mat.get_nb_lignes(matrice)
        mat_transposee = mat.construit_matrice(larg, long, None)
        for ligne in range(larg):
            for colonne in range(long):
                mat.set_val(mat_transposee, ligne, colonne, mat.get_val(matrice, colonne, ligne))
        return mat_transposee
    else:
        return "Il faut une matrice carré"

def is_triangulaire_inf(matrice):
    """Indique si c'est une matrice triangulaire inférieur

    Args:
        matrice (list): matrice quelconque

    Returns:
        bool: Indique si c'est une matrice triangulaire inférieur ou pas
    """
    if mat.get_nb_lignes(matrice) == mat.get_nb_colonnes(matrice):
        for ind in range(mat.get_nb_lignes(matrice)-1):
            for ind2 in range(1+ind, mat.get_nb_lignes(matrice)-1):
                if mat.get_val(matrice, ind, ind2) != 0:
                    return False
        return True
    else:
        return "Il faut une matrice carré"

def is_triangulaire_sup(matrice):
    """Indique si c'est une matrice triangulaire supérieur

    Args:
        matrice (list): matrice quelconque

    Returns:
        bool: Indique si c'est une matrice triangulaire supérieur ou pas
    """
    if mat.get_nb_lignes(matrice) == mat.get_nb_colonnes(matrice):
        for ind in range(mat.get_nb_lignes(matrice)-1):
            for ind2 in range(1+ind, mat.get_nb_lignes(matrice)-1):
                if mat.get_val(matrice, ind2, ind) != 0:
                    return False
        return True
    else:
        return "Il faut une matrice carré"

def bloc(matrice , ligne , colonne , hauteur , largeur):
    """Renvoie la sous-matrice de la matrice commençant à la ligne et colonne 
    indiquées et dont les dimensions sont hauteur et largeur.

    Args:
        matrice (list): matrice quelconque
        ligne (int): Ligne de début
        colonne (int): Colonne de début
        hauteur (int): Hauteur de la matrice souhaitée
        largeur (int): Largeur de la matrice souhaitée

    Returns:
        list: Matrice voulue
    """
    part_matrice = []
    return part_matrice
