def algo2 (mot) :
    """Indiche le nombre de voyelles

    Args:
        mot (str): Un mot en minuscule
    Returns:
        int: nombre de voyelle
    """

    res=0

    for lettre in mot:
        if lettre in 'aeiouy':
            res += 1
        else:
            res += -1
    return res>0

def test_algo2():
    assert algo2("manger") == False
    assert algo2("ici") == 1
