def algo1 (a,b,c,d) :
    """Va définir le lus petit nombre parmi les 4 donnés.

    Args:
        a (int): Un nbr aléatoire
        b (int): Un nbr aléatoire
        c (int): Un nbr aléatoire
        d (int): Un nbr aléatoire

    Returns:
        res (int): Le plus petit nombre parmi les 4.
    """
    if a<b :
        res=a
    else :
        res=b
    if c<res :
        res=c
    if d<res :
        res=d
    return res

def test_algo1 () :
    assert algo1 (4,7,12,4)==4
    assert algo1 (14,2,15,48)==2
    assert algo1 (4,8,1,3)==1
    assert algo1 (-2,7,8,9)==-2