"""TP7 une application complète
ATTENTION VOUS DEVEZ METTRE DES DOCSTRING A TOUTES VOS FONCTIONS
"""
def afficher_menu(titre, liste_options):
    """Affiche un menu à partir d'un titre et d'une liste de fct

    Args:
        titre (str): Titre du menu
        liste_options (list): list de str qu'on souhaite afficher
    """
    print("+"+"-"*(len(titre)+2)+"+")
    print("| "+titre+" |")
    print("+"+"-"*(len(titre)+2)+"+")
    for ind in range(len(liste_options)):
        print(str(ind+1)+" -> "+liste_options[ind])
#afficher_menu("Mon Titre", ["Option 1", "Option 2"])

def demander_nombre(message, borne_max):
    """Demande un nombre et retourne celui-ci ou None s'il est dans l'interval [1,borne_max] ou pas

    Args:
        message (str): Message d'invitation à l'écriture d'un nombre
        borne_max (int): Valeur maximal accepté pour retourner le nombre

    Returns:
        int: Le nombre s'il est dans l'interval sinon None.
    """

    val = int(input(message))
    if val >= 1 and val <= borne_max:
        return val
    return None
    
#print(demander_nombre("Donnez un nombre : ", 4))

def menu(titre, liste_options):
    """Ecris un menu et donne le numéro de l'option qu'on aura demandée

    Args:
        titre (str): Titre du menu
        liste_options (list): list de str qu'on souhaite afficher

    Returns:
        int:  numéro de l'option demandée
    """
    afficher_menu(titre, liste_options)
    num_opt = demander_nombre("Quelle option souhaitez-vous ? ", len(liste_options))
    return num_opt
#menu("Mon Titre", ["Option 1", "Option 2"]))

def programme_principal():
    liste_options = ["Charger un fichier", "Rechercher la population d'une commune", "Afficher la population d'un département", "Quitter"]
    while True:
        rep = menu("MENU DE MON APPLICATION", liste_options)
        if rep is None:
            print("Cette option n'existe pas")
        elif rep == 1:
            print("Vous avez choisi", liste_options[rep-1])
        elif rep == 2:
            print("Vous avez choisi", liste_options[rep-1])
        elif rep == 3:
            print("Vous avez choisi", liste_options[rep-1])
        else:
            break
        input("Appuyer sur Entrée pour continuer")
    print("Merci au revoir!")
#programme_principal()

#Exercice 2.1
def charger_fichier_population(nom_fic):
    """Charge un fichier CSV en une liste de tuples

    Args:
        nom_fic (str): Chemin d'accès au fichier CSV

    Returns:
        list: liste de tuples des villes
    """
    liste_villes = []
    try:
        fic = open(nom_fic, "r")
        fic.readline()
        for ligne in fic:
            ville_cour = ligne.split(";")
            liste_villes.append((ville_cour[0], ville_cour[1], int(ville_cour[4])))
        fic.close()
        return liste_villes
    except:
        return "Ce fichier n'existe pas"

#charger_fichier_population("Tp7/extrait1.csv")




#Exercice 2.3
def population_d_une_commune(liste_pop,nom_commune):
    """Donne la population d'un commune grace à son nom

    Returns:
        int: nbr d'habitants de la commune
    """
    for ville in liste_pop:
        if ville[1] == nom_commune:
            return ville[2]
    return None

#Exercice 2.4
def liste_des_communes_commencant_par(liste_villes,debut_nom):
    """Donne une liste des commune commençant pas un certain début

    Args:
        liste_villes (list): Liste de villes (tuples)
        debut_nom (str): une chaine de caractère 

    Returns:
        list: Liste des commune commençant pas un certain début
    """
    liste_villes_debut = []
    for ville in liste_villes:
        if ville[1].startswith(debut_nom):
            liste_villes_debut.append(ville)
    return liste_villes_debut

#Exercice 2(.2) :
def programme_principal_plus():
    liste_options = ["Charger un fichier", "Rechercher la population d'une commune", "Afficher la population d'un département", "Afficher les villes commençant par...","Afficher la commune la plus peuplée d'un département", "Afficher les villes dans une borne donnée", "Quitter"]
    while True:
        rep = menu("MENU DE MON APPLICATION", liste_options)
        if rep is None:
            print("Cette option n'existe pas")
        elif rep == 1:
            nom_fic = "Tp7/"+input("Quel fichier souhaitez-vous charger ? ")
            try:
                liste_villes = charger_fichier_population(nom_fic)  
                print("Ce fichier contient "+str(len(liste_villes))+" communes.")      
            except:
                return "Ce fichier n'existe pas"
        elif rep == 2:
            try:
                ville = input("De quelle ville voulez-vous savoir le nbr d'habitants ? ")
                pop_com = population_d_une_commune(liste_villes, ville)
                print(ville+" possède "+str(pop_com)+"habitants")
            except:
                print("Erreur : Soit le fichier n'a pas été chargé, soit vous avez donné un nom non valide ou pas dans la liste")
        elif rep == 3:
            print("Vous avez choisi", liste_options[rep-1])
        elif rep == 4:
            debut_nom = input("Quelle est le nom du début de votre ville ? ")
            liste_ville_debut = liste_des_communes_commencant_par(liste_villes,debut_nom)
            print("Voici les villes commençant par ce que vous m'avez donné :")
            print(liste_ville_debut)
        elif rep == 5:
            num_dep = input("Donner le numéro du département")
            com_top = commune_plus_peuplee_departement(liste_villes, num_dep)
            print("La commune la plus peuplée du "+num_dep+" est :")
            print(com_top)
        elif rep == 6:
            com_dep = input("Donnez le département de la commune à introduire")
            com_nom = input("Donnez le nom de la commune à introduire")
            com_dep = input("Donnez la population de la commune à introduire"))
            commune = (com_dep, com_nom, int(com_dep))
            
        else:
            break
        input("Appuyer sur Entrée pour continuer")
    print("Merci au revoir!")

#programme_principal_plus()

#DEPCOM;COM;PMUN;PCAP;PTOT
#NumDepartement, Nom commune, Pop principale, Pop à part, Pop Tot
#(DEPCOM,COM,PTOT)

#Exercice 2.5
def commune_plus_peuplee_departement(liste_pop, num_dpt):
    """Donne la commune la plus peuplée d'un département.

    Args:
        liste_pop (list): Liste de tuples des villes
        num_dpt (str): Numéro du département

    Returns:
        tuple: La commune la plus peuplée d'un département
    """
    max_pop = None
    for ind in range(len(liste_pop)):
        if liste_pop[ind][0].startswith(num_dpt):
            if max_pop == None or liste_pop[ind][2] > max_pop:
                max_pop = liste_pop[ind][2]
                ind_max = ind
    if max_pop == None:
        com_top = None
    else:
        com_top = liste_pop[ind_max]
    return com_top

#exercice 2.6
def nombre_de_communes_tranche_pop(liste_pop,pop_min,pop_max):
    """Donne la liste des communes dans un borne de population.

    Args:
        liste_pop (list): Liste de tuples des villes
        pop_min (int): La borne minimale de la tranche
        pop_max (int): La borne maximale de la tranche

    Returns:
        list: La liste des communes dans un borne de population
    """
    com_tranches = []
    for ville in liste_pop:
        if ville[2] > pop_min and ville[2] < pop_max:
            com_tranches.append(ville)
    return com_tranches

def place_top(commune, liste_pop):
    """Donne l'indice auquel la commune doit s'insérer

    Args:
        commune ([type]): [description]
        liste_pop ([type]): [description]

    Returns:
        [type]: [description]
    """
    return ind_ins
    
#exercice 2.7
def ajouter_trier(commune,liste_pop,taille_max):
    """Insère une ville dans une liste de villes triées

    Args:
        commune (tuple): tuple représentant une ville
        liste_pop (list): La liste des communes dans un borne de population
    """
    inser = True
    ind = 0
    while ind < len(liste_pop) and inser == True:
        if commune[2] > liste_pop[ind][2]:
            liste_pop.insert(ind, commune)
            inser = False
        else:
            ind += 1
    
    

'''def top_n_population(liste_pop,nb):
    ...

def population_par_departement(liste_pop):
    ...

def sauve_population_dpt(nom_fic,liste_pop_dep):
    ...

# appel au programme principal
programme_principal()
'''
