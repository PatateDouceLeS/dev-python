"""Fonctions de test du TP7
ATTENTION VOUS DEVEZ COMPLETER LES TESTS
pour appeler vos fonctions ecrivez: tp7_source.ma_fonction
"""
import tp7_source as tp7

LISTE_COMMUNES = [('18001', 'aaa', 1200), ('18002', 'bbb', 71200), ('18003', 'ccc', 520),
                ('45001', 'ddd', 85200), ('45002', 'abcd', 6350), ('45003', 'aaa sur Loire', 8534), ('45004', 'ggg', 5201)]

LISTE_COMMUNES_TRIE = [('45001', 'ddd', 85200), ('18002', 'bbb', 71200), ('45003', 'aaa sur Loire', 8534),
                    ('45002', 'abcd', 6350), ('45004', 'ggg', 5201), ('18001', 'aaa', 1200), ('18003', 'ccc', 520)]

def test_charger_fichier_population():
    """Test la fonction : "charger_fichier_population"
    """
    assert tp7.charger_fichier_population("") == "Ce fichier n'existe pas"
    assert tp7.charger_fichier_population("Tp7/extrait1.csv") == [('45232', 'Olivet', 22474), ('45233', 'Ondreville-sur-Essonne', 416), ('45234', 'Orléans', 119085), ('45235', 'Ormes', 4200), ('51204', 'Damery', 1464), ('51205', 'Dampierre-au-Temple', 279), ('51206', 'Dampierre-le-Château', 108), ('51208', 'Dampierre-sur-Moivre', 115), ('77345', 'Orly-sur-Morin', 688), ('77347', 'Les Ormes-sur-Voulzie', 868), ('77348', 'Ormesson', 249)]


def test_population_d_une_commune():
    assert tp7.population_d_une_commune(LISTE_COMMUNES, 'aaa') == 1200
    assert tp7.population_d_une_commune(LISTE_COMMUNES, '') is None
    assert tp7.population_d_une_commune(LISTE_COMMUNES, 'Tours') is None


def test_liste_des_communes_commencant_par():
    assert tp7.liste_des_communes_commencant_par(LISTE_COMMUNES, 'aaa') == [('18001', 'aaa', 1200), ('45003', 'aaa sur Loire', 8534)]
    assert tp7.liste_des_communes_commencant_par(LISTE_COMMUNES, 'ab') == [('45002', 'abcd', 6350)]
    assert tp7.liste_des_communes_commencant_par(LISTE_COMMUNES, ' ') == []


def test_commune_plus_peuplee_departement():
    assert tp7.commune_plus_peuplee_departement([], "") is None
    assert tp7.commune_plus_peuplee_departement(LISTE_COMMUNES, "45") == ('45001', 'ddd', 85200)
    assert tp7.commune_plus_peuplee_departement(LISTE_COMMUNES, "37") is None
    assert tp7.commune_plus_peuplee_departement(LISTE_COMMUNES, "18") == ('18002', 'bbb', 71200)

def test_nombre_de_communes_tranche_pop():
    assert tp7.nombre_de_communes_tranche_pop([], None, None) == []
    assert tp7.nombre_de_communes_tranche_pop(LISTE_COMMUNES, 0, 500) == []
    assert tp7.nombre_de_communes_tranche_pop(LISTE_COMMUNES, 90000, 100000) == []
    assert tp7.nombre_de_communes_tranche_pop(LISTE_COMMUNES, 70000, 86000) == [('18002', 'bbb', 71200), ('45001', 'ddd', 85200)]

'''def test_ajouter_trier():
    ...
    

def test_top_n_population():
    ...

def test_population_par_departement():
    ...
'''
