# -*-coding:Latin-1 -*-
import motdepasse

# --------------------------------------
# FONCTIONS
# --------------------------------------

def test_longueur_ok():
    assert motdepasse.longueur_ok("choubouilli") # longueur ok
    assert not motdepasse.longueur_ok("chou") # longueur pas ok
    assert not motdepasse.longueur_ok("") # chaine vide


def test_chiffre_ok():
    assert not motdepasse.chiffre_ok("") # chaine vide
    assert motdepasse.chiffre_ok("chou9bouilli")  # chiffre au milieu
    assert motdepasse.chiffre_ok("7choubouilli")  # chiffre au début
    assert motdepasse.chiffre_ok("choubouilli5")  # chiffre à la fin
    assert motdepasse.chiffre_ok("chou3boui8lli")  # deux chiffres    
    assert not motdepasse.chiffre_ok("chou")       # pas de chiffres
    assert not motdepasse.chiffre_ok("un deux trois") # pas de chiffres

def test_sans_espace():
    assert motdepasse.sans_espace("") # chaine vide
    assert motdepasse.sans_espace("choubouilli") # sans espace ok
    assert not motdepasse.sans_espace("chou bouilli") # espace au milieu
    assert not motdepasse.sans_espace(" choubouilli") # espace au début
    assert not motdepasse.sans_espace("choubouilli ") # espace à la fin
    assert motdepasse.sans_espace("") # chaine vide

def test_chiffre_ok_x3():
    assert not motdepasse.chiffre_ok_x3("")# chaine vide
    assert not motdepasse.chiffre_ok_x3("chou9bou7illi")  # chiffre au milieu
    assert motdepasse.chiffre_ok_x3("74choubouilli4")  # chiffre au début
    assert motdepasse.chiffre_ok_x3("4choubo1uilli1")  # chiffre à la fin
    assert not motdepasse.chiffre_ok_x3("chou3boui8lli")  # deux chiffres    
    assert not motdepasse.chiffre_ok_x3("chou")       # pas de chiffres
    assert not motdepasse.chiffre_ok_x3("un deux trois") # pas de chiffres


def test_sans_2chiffres_conseq():
    assert motdepasse.sans_2chiffres_conseq("") # chaine vide
    assert motdepasse.sans_2chiffres_conseq("chou9bou7illi")  # chiffre au milieu
    assert not motdepasse.sans_2chiffres_conseq("74choubouilli4")  # chiffre au début et à la fin
    assert not motdepasse.sans_2chiffres_conseq("choubo441uilli")  # chiffre au milieu
    assert not motdepasse.sans_2chiffres_conseq("chou3boui8lli88")  # deux chiffres    
    assert motdepasse.sans_2chiffres_conseq("chou")       # pas de chiffres
    assert motdepasse.sans_2chiffres_conseq("un deux trois") # pas de chiffres


def test_chiffre_min():
    assert motdepasse.chiffre_min("") # chaine vide
    assert motdepasse.chiffre_min("chou4bou4illi1")  # chiffres au milieu
    assert not motdepasse.chiffre_min("74choubouilli4")  # chiffre au début et à la fin
    assert motdepasse.chiffre_min("choubo441uilli")  # chiffre au milieu
    assert motdepasse.chiffre_min("chou3boui8lli88") # plein de chiffres mais ça passe
    assert not motdepasse.chiffre_min("chou148515") # pas de chiffres
    assert motdepasse.chiffre_min("un deux trois") # pas de chiffres 