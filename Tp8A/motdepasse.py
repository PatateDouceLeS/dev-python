# -*-coding:Latin-1 -*-
# Codé par Papy Force X, jeune padawan de l'informatique

def ancien_dialogue_mot_de_passe():
    """Vérifie l'éligibilité d'un mdp avec une légère interface utilisateur

    Returns:
        bool: booléen indiquant l'éligibilité du mdp
    """
    login = input("Entrez votre nom : ")
    mot_de_passe_correct = False
    while not mot_de_passe_correct :
        mot_de_passe = input("Entrez votre mot de passe : ")
        # je vérifie la longueur
        if len(mot_de_passe) < 8:
            longueur_ok = False;
        else:
            longueur_ok = True
        # je vérifie s'il y a un chiffre
        chiffre_ok = False
        for lettre in mot_de_passe:
            if lettre.isdigit():
                chiffre_ok = True
        # je vérifie qu'il n'y a pas d'espace
        sans_espace = True
        for lettre in mot_de_passe:
            if lettre == " ":
                sans_espace = False
        # Je gère l'affichage
        if not longueur_ok:
            print("Votre mot de passe doit comporter au moins 8 caractères")
        elif not chiffre_ok:
            print("Votre mot de passe doit comporter au moins un chiffre")
        elif not sans_espace:
            print("Votre mot de passe ne doit pas comporter d'espace")	   
        else:
            mot_de_passe_correct = True        
    print("Votre mot de passe est correct")
    return mot_de_passe


def longueur_ok(mdp):
    """Indique si le mdp est assez long

    Args:
        mdp(str): Un mot de passe

    Returns:
        bool: validité de la longueur

    """
    return len(mdp) >= 8

def chiffre_ok(mdp):
    """Vérifie s'il y a un chiffre dans le mdp

    Args:
        mdp (str): Un mot de passe

    Returns:
        bool: validité du mdp sur ce critère
    """
    ind = 0
    valide = False
    while ind < len(mdp) and not valide:
        if mdp[ind].isdigit():
            valide = True
        else:
            ind += 1
    return valide

def sans_espace(mdp):
    """Vérifie s'il n'y a pas d'espace dans le mdp

    Args:
        mdp (str): Un mot de passe

    Returns:
        bool: validité du mdp sur ce critère
    """
    ind = 0
    valide = True
    recherche = True
    while ind < len(mdp) and recherche:
        if mdp[ind] == " ":
            recherche = False
            valide = False
        else:
            ind += 1        
    return valide

def dialogue_mot_de_passe():
    """Vérifie l'éligibilité d'un mdp avec une légère interface utilisateur

    Returns:
        bool: booléen indiquant l'éligibilité du mdp
    """
    login = input("Entrez votre nom : ")
    mot_de_passe_correct = False
    while not mot_de_passe_correct :
        mot_de_passe = input("Entrez votre mot de passe : ")
        if longueur_ok(mot_de_passe): # je vérifie la longueur
            if chiffre_ok(mot_de_passe): # je vérifie s'il y a un chiffre
                if sans_espace(mot_de_passe): # je vérifie qu'il n'y a pas d'espace
                    mot_de_passe_correct = True
                else:
                    print("Votre mot de passe ne doit pas comporter d'espace")
            else:
                print("Votre mot de passe doit comporter au moins un chiffre")
        else:
            print("Votre mot de passe doit comporter au moins 8 caractères")  
    print("Votre mot de passe est correct")
    return mot_de_passe

#dialogue_mot_de_passe()

#Exercice 2:

def chiffre_ok_x3(mdp):
    """Vérifie s'il y a un chiffre dans le mdp

    Args:
        mdp (str): Un mot de passe

    Returns:
        bool: Validité du mdp sur ce critère
    """
    ind = 0
    valide = False
    nbr_chiffre = 0
    while ind < len(mdp) and not valide:
        if mdp[ind].isdigit():
            nbr_chiffre += 1
            if nbr_chiffre == 3:
                valide = True
        ind += 1
    return valide

def sans_2chiffres_conseq(mdp):
    """Vérifie s'il n'y a pas deux chiffres consécutifs dans le mdp

    Args:
        mdp (str): Un mot de passe

    Returns:
        bool: Validité du mdp sur ce critère
    """
    ind = 0
    valide = True
    prec_chiffre = False
    while ind < len(mdp) and valide:
        if mdp[ind].isdigit():
            if prec_chiffre:
                valide = False
            else:
                prec_chiffre = True
        else:
            prec_chiffre = False
        ind += 1
    return valide

def chiffre_min(mdp):
    """Vérifie que le le plus petit chiffre n'apparait qu'une seule fois

    Args:
        mdp (str): Un mot de passe

    Returns:
        bool: Validité du mdp sur ce critère
    """
    ind = 0
    min = None
    nbr_min = 0
    while ind < len(mdp):
        if mdp[ind].isdigit():
            if mdp[ind] == min:
                nbr_min += 1
            elif min == None or mdp[ind] < min:
                min = mdp[ind]
                nbr_min = 1
        ind+=1
    return nbr_min < 2

def palpa_dialogue_mot_de_passe():
    """Vérifie l'éligibilité d'un mdp avec une légère interface utilisateur

    Returns:
        bool: booléen indiquant l'éligibilité du mdp
    """
    login = input("Entrez votre nom : ")
    mot_de_passe_correct = False
    while not mot_de_passe_correct :
        mot_de_passe = input("Entrez votre mot de passe : ")
        if longueur_ok(mot_de_passe): # je vérifie la longueur
            if chiffre_ok_x3(mot_de_passe): # je vérifie s'il y a un chiffre
                if sans_espace(mot_de_passe): # je vérifie qu'il n'y a pas d'espace
                    if sans_2chiffres_conseq(mot_de_passe):
                        if chiffre_min(mot_de_passe):
                            mot_de_passe_correct = True
                        else:
                            print("Votre mot de passe ne doit comporter qu'une seule fois le chiffre minimum")
                    else:
                        print("Votre mot de passe ne doit pas comporter 2 chiffres sonsécutifs")
                else:
                    print("Votre mot de passe ne doit pas comporter d'espace")
            else:
                print("Votre mot de passe doit comporter au moins trois chiffres")
        else:
            print("Votre mot de passe doit comporter au moins 8 caractères")  
    print("Votre mot de passe est correct")
    return mot_de_passe

#palpa_dialogue_mot_de_passe()

