import oiseaux
# --------------------------------------
# FONCTIONS
# --------------------------------------

def test_recherche_oiseau():
    assert oiseaux.recherche_oiseau([], "") is None
    assert oiseaux.recherche_oiseau(oiseaux.OISEAUX, "Rouge-gorge") == "Passereau"
    assert oiseaux.recherche_oiseau(oiseaux.OISEAUX, "Corbeau") is None


def test_recherche_par_famille():
    assert oiseaux.recherche_par_famille([], "") == []
    assert oiseaux.recherche_par_famille(oiseaux.OISEAUX, "Passereau") == [("Mésange", "Passereau"), ("Moineau", "Passereau"), ("Pinson", "Passereau"), ("Rouge-gorge", "Passereau")]
    assert oiseaux.recherche_par_famille(oiseaux.OISEAUX, "Corvidé") == [("Pie", "Corvidé")]
    assert oiseaux.recherche_par_famille(oiseaux.OISEAUX, "Canidé") == []


def test_oiseau_le_plus_observe():
    assert oiseaux.oiseau_le_plus_observe(oiseaux.OBSERVATIONS1) == "Moineau"
    assert oiseaux.oiseau_le_plus_observe(oiseaux.OBSERVATIONS2) == "Tourterelle"
    assert oiseaux.oiseau_le_plus_observe(oiseaux.OBSERVATIONS3) == "Mésange"
    assert oiseaux.oiseau_le_plus_observe([]) == None


def test_ordre_alpha():
    assert oiseaux.ordre_alpha([])
    assert not oiseaux.ordre_alpha([("Moineau", 2),  ("Merle", 5)])
    assert oiseaux.ordre_alpha(oiseaux.OBSERVATIONS1)

def test_observ_non_nulle():
    assert oiseaux.observ_non_nulle([])
    assert not oiseaux.observ_non_nulle([("Merle", 2),  ("Moineau", 0)])
    assert oiseaux.observ_non_nulle(oiseaux.OBSERVATIONS1)

def test_est_liste_observations():
    assert not oiseaux.est_liste_observations([])
    assert not oiseaux.est_liste_observations([("Merle", 2),  ("Moineau", 0)])
    assert not oiseaux.est_liste_observations([("Moineau", 2),  ("Merle", 5)])
    assert oiseaux.est_liste_observations(oiseaux.OBSERVATIONS1)


def test_max_observations():
    assert oiseaux.max_observations([]) is None
    assert oiseaux.max_observations(oiseaux.OBSERVATIONS1) == 5
    assert oiseaux.max_observations(oiseaux.OBSERVATIONS2) == 5
    assert oiseaux.max_observations(oiseaux.OBSERVATIONS3) == 4


def test_moyenne_oiseaux_observes():
    assert oiseaux.moyenne_oiseaux_observes([]) is None
    assert oiseaux.moyenne_oiseaux_observes(oiseaux.OBSERVATIONS1) == 3
    assert oiseaux.moyenne_oiseaux_observes(oiseaux.OBSERVATIONS2) == 2.5
    assert oiseaux.moyenne_oiseaux_observes(oiseaux.OBSERVATIONS3) == 2.5


def test_attrib_famille():
    assert oiseaux.attrib_famille([], "") == []
    assert oiseaux.attrib_famille(oiseaux.OISEAUX, "") == []
    assert oiseaux.attrib_famille(oiseaux.OISEAUX, "Turtidé") == ["Merle"]
    assert oiseaux.attrib_famille(oiseaux.OISEAUX, "Passereau") == ["Mésange", "Moineau", "Pinson", "Rouge-gorge"]


def test_total_famille():
    assert oiseaux.total_famille([], [], "") is None
    assert oiseaux.total_famille(oiseaux.OISEAUX, oiseaux.OBSERVATIONS1, "Turtidé") == 2
    assert oiseaux.total_famille(oiseaux.OISEAUX, oiseaux.OBSERVATIONS2, "Hey !") is None
    assert oiseaux.total_famille(oiseaux.OISEAUX, oiseaux.OBSERVATIONS3, "Passereau") == 7
    

def test_construire_liste_observations():
    assert oiseaux.construire_liste_observations(oiseaux.OISEAUX, []) == []
    assert oiseaux.construire_liste_observations(oiseaux.OISEAUX, oiseaux.COMPTAGE1) == oiseaux.OBSERVATIONS1
    assert oiseaux.construire_liste_observations(oiseaux.OISEAUX, oiseaux.COMPTAGE2) == oiseaux.OBSERVATIONS2
    assert oiseaux.construire_liste_observations(oiseaux.OISEAUX, oiseaux.COMPTAGE3) == oiseaux.OBSERVATIONS3



