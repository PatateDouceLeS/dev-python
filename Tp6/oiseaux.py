# --------------------------------------
# DONNEES
# --------------------------------------

# exemple de liste d'oiseaux observables
OISEAUX = [
    ("Merle", "Turtidé"), ("Mésange", "Passereau"), ("Moineau", "Passereau"),
    ("Pic vert", "Picidae"), ("Pie", "Corvidé"), ("Pinson", "Passereau"),
    ("Tourterelle", "Colombidé"), ("Rouge-gorge", "Passereau")
]
# exemples de listes de comptage ces listes ont la même longueur que oiseaux
COMPTAGE1 = [2, 0, 5, 1, 2, 0, 5, 3]
COMPTAGE2 = [2, 1, 3, 0, 0, 3, 5, 1]
COMPTAGE3 = [0, 4, 0, 2, 2, 1, 4, 2]

# exemples de listes d'observations. Notez que chaque liste correspond à la liste de comptage de
# même numéro
OBSERVATIONS1 = [
    ("Merle", 2),  ("Moineau", 5), ("Pic vert", 1), ("Pie", 2),
    ("Tourterelle", 5), ("Rouge-gorge", 3)
]

OBSERVATIONS2 = [
    ("Merle", 2), ("Mésange", 1), ("Moineau", 3),
    ("Pinson", 3), ("Tourterelle", 5), ("Rouge-gorge", 1)
]

OBSERVATIONS3 = [
    ("Mésange", 4), ("Pic vert", 2), ("Pie", 2), ("Pinson", 1),
    ("Tourterelle", 4), ("Rouge-gorge", 2)
]

# --------------------------------------
# FONCTIONS
# --------------------------------------

#Exercice 1.2
def oiseau_le_plus_observe(liste_observations):
    """ recherche le nom de l'oiseau le plus observé de la liste (si il y en a plusieur on donne le 1er trouve)

    Args:
        liste_observations (list): une liste de tuples (nom_oiseau,nb_observes)

    Returns:
        str: l'oiseau le plus observé (None si la liste est vide)
    """
    oiseau_max = None
    for observation in liste_observations:
        if oiseau_max is None:
            oiseau_max = observation
        if observation[1] > oiseau_max[1]:
            oiseau_max = observation
    if oiseau_max is None:
        return None
    return oiseau_max[0]


#Exercice 1.3
def oiseau_le_plus_observe_ind(liste_observations):
    """ recherche le nom de l'oiseau le plus observé de la liste (si il y en a plusieur on donne le 1er trouve)

    Args:
        liste_observations (list): une liste de tuples (nom_oiseau,nb_observes)

    Returns:
        str: l'oiseau le plus observé (None si la liste est vide)
    """
    oiseau_max = None
    for ind in range(len(liste_observations)):
        if oiseau_max is None:
            oiseau_max = liste_observations[ind]
        if liste_observations[ind][1] > oiseau_max[1]:
            oiseau_max = liste_observations[ind]
    if oiseau_max is None:
        return None
    return oiseau_max[0]


#Exercice 2.1
def recherche_oiseau(oiseaux, nom_oiseau):
    """Donne l'espece d'un oiseau avec son nom

    Args:
        oiseaux (list): Liste contenant les oiseaux et leur famille
        nom_oiseau (str): nom de l'oiseau recherché

    Returns:
        str: famille de l'oiseau recherché
    """
    for elem in oiseaux:
        if elem[0] == nom_oiseau:
            return elem[1]
    return None


#Exercice 2.2
def recherche_par_famille(oiseaux, famille):
    """Recherche les oiseaux appartenant à une famille

    Args:
        oiseaux (list): Liste contenant les oiseaux et leur famille
        famille (str): Famille recherchée

    Returns:
        list: Liste des oiseaux appartenant à la famille
    """
    liste = []
    for elem in oiseaux:
        if elem[1] == famille:
            liste.append(elem)
    return liste


#Exercice 3.1
def est_liste_observations(li_obs):
    """Indique si une liste est valable ou pas

    Args:
        li_obs (list): liste des observations d'oiseaux

    Returns:
        bool: indique la validité
    """
    if ordre_alpha(li_obs) and observ_non_nulle(li_obs) and len(li_obs) != 0:
        return True
    return False

def ordre_alpha(li_obs):
    """Indique si la liste est trié par ordre alphabétique

    Args:
        li_obs (list): liste des observations d'oiseaux

    Returns:
        bool: indique la validité de la liste sur ce point
    """
    prec = None
    for cour in li_obs:
        if prec is None:
            prec = cour
        else:
            if cour < prec:
                return False
    return True

def observ_non_nulle(li_obs):
    """Indique si la liste contient des observation de 0 oiseau

    Args:
        li_obs (list): liste des observations d'oiseaux

    Returns:
        bool: indique la validité de la liste sur ce point
    """
    for elem in li_obs:
        if elem[1] == 0:
            return False
    return True


#Exercice 3.2
def max_observations(li_obs):
    """Donne le plus grand nombre de spécimen d'oiseau observé

    Args:
        li_obs (list): liste des observations d'oiseaux

    Returns:
        int: Le plus grand nombre de spécimen d'oiseau observé
    """
    max = None
    if est_liste_observations(li_obs):
        for elem in li_obs:
            if max == None or elem[1] > max:
                max = elem[1]
    return max


#Exercice 3.3
def moyenne_oiseaux_observes(li_obs):
    """Donne la moyenne de spécimens observés

    Args:
        li_obs (list): liste des observations d'oiseaux

    Returns:
        float: Moyenne de spécimens observés
    """
    cpt = 0
    moy = None
    if est_liste_observations(li_obs):
        for elem in li_obs:
            if cpt == 0:
                som = elem[1]
            else:
                som += elem[1]
            cpt += 1
        moy = som / cpt
    return moy


#Exercice 3.4
def attrib_famille(oiseaux, famille):
    """Donne la liste des oiseaux appartenant à une famille

    Args:
        oiseaux (list): Répertoire d'oiseaux
        famille (str): Famille d'oiseaux

    Returns:
        list: Liste des oiseaux appartenant à une famille
    """
    liste = []
    for elem in oiseaux:
        if elem[1] == famille:
            liste.append(elem[0])
    return liste

def total_famille(oiseaux, li_obs, famille):
    """Donne le nombre d'oiseaux d'une espèce observé

    Args:
        oiseaux (list): Répertoire d'oiseaux
        li_obs (list): liste des observations d'oiseaux 
        famille (list): répertoire d'oiseaux

    Returns:
        int: nombre total d'oiseaux d'une espèce observé
    """
    tot_espece = None
    liste = attrib_famille(oiseaux, famille)
    if est_liste_observations(li_obs):
        for elem in li_obs:
            if tot_espece == None and elem[0] in liste:
                tot_espece = elem[1]
            else:
                if elem[0] in liste:
                    tot_espece += elem[1]
    return tot_espece

#Exercice 4.1
def construire_liste_observations(oiseaux, comptage):
    """Construit une liste d'observation à partir d'un comptage

    Args:
        oiseaux (list): Répertoire d'oiseaux
        comptage (list): Liste d'entier comptant le nombre de chaque oiseau observé

    Returns:
        list: Une liste d'observation viable
    """
    li_obs = []
    for ind in range(len(comptage)):
        if comptage[ind] != 0:
            li_obs.append((oiseaux[ind][0], comptage[ind]))
    return li_obs

#Exercice 4.2
def li_obs_interact(oiseaux):
    """Demande à l’utilisateur le nombre de spécimens de chaque oiseau qu’il a observé et en fait une liste d'observation.

    Args:
        oiseaux (list): Répertoire d'oiseaux

    Returns:
        list: Une liste d'observation viable
    """
    comptage = []
    for (specimen, _) in oiseaux:
        comptage.append(int(input("Combien avez vous vu de "+specimen+" : ")))
        li_obs = construire_liste_observations(oiseaux, comptage)
    return li_obs

print(li_obs_interact(OISEAUX))
#nbr_obs = int(input("Combien avez vous vu de "+specimen+" : "))

# --------------------------------------
# PROGRAMME PRINCIPAL.

# --------------------------------------

# afficher_graphique_observation(construire_liste_observations(oiseaux,comptage3))
# comptage=saisie_observations(oiseaux)
# afficher_graphique_observation(comptage)
# afficher_observations(comptage,oiseaux)
