"""TP 14"""

# EXERCICE 1

def difficulté(recette):
    return recette[1][1]

def recette_la_plus_facile(menu):
    """
    Résultat : le nom de la recette qui est la plus facile
    
    """
    if menu != dict():
        recette_facile = min(menu.items(), key = difficulté)
        return recette_facile[0]
    return None



def temps_total_de_preparation(menu):
    """
    Résultat : renvoie le temps total de préparation des recettes du menu
    """
    temps_tot = 0
    for (_, (temps, _)) in menu.items():
        temps_tot += temps
    return temps_tot



def menu_de_chef(menu):
    """
    Résultat : vérifie si le menu contient au moins une recette
    difficile (difficulté > 5)
    
    """
    for (_, (_, dif)) in menu.items():
        if dif > 5:
            return True
    return False



def temps(recette):
    return recette[1][0]

def recette_la_plus_longue(menu):
    """
    Résultat : le nom de la recette qui demande le plus de temps
    de préparation et cuisson
    
    """
    if menu != dict():
        recette_longue = max(menu.items(), key = temps)
        return recette_longue[0]
    return None


def recettes_triee_par_temps(menu):
    """
    Résultat : la liste des noms de recettes triées par ordre croissant
    de temps de préparation    
    """
    recette_dif = []
    menu_trie = sorted(menu.items(), key = temps)
    for (nom, _) in menu_trie:
        recette_dif.append(nom)
    return recette_dif


def recettes_triee_par_difficulte(menu):
    """
    Résultat : la liste des noms de recettes triées par ordre croissant
    de difficulté 
    """
    recette_dif = []
    menu_trie = sorted(menu.items(), key = difficulté)
    for (nom, _) in menu_trie:
        recette_dif.append(nom)
    return recette_dif


# EXERCICE 2

def somme(liste):
    total = 0
    for elem in liste:
        total += elem
    return total

def reglement(prix, porte_monnaie):
    porte_monnaie_trie = sorted(porte_monnaie, reverse = True)
    paiement = []
    prix_cour = prix
    for argent in porte_monnaie_trie:
        if argent <= prix_cour:
            paiement.append(argent)
            prix_cour -= argent
            if prix_cour == 0:
                return paiement
    return None

#On pourrait utiliser un dico de freq pr le porte-monnaie

# EXERCICE 3

def duellistes(dresseurs):
    """
    parametre: dresseurs est un dictionnaires :
    - clé : le nom du dresseur (str)
    - valeur : son classement Elo (int)
    résultat : renvoie le nom des deux dresseurs qui ont les classements
    Elo les plus proches
    """
    dif_min = None
    combattants = set()
    def elo(tuple):
        return tuple[1]
    tri_par_elo = sorted(dresseurs.items(), key = elo)
    for ind in range(1, len(tri_par_elo)):
        (nom1, elo1) = tri_par_elo[ind]
        (nom2, elo2) = tri_par_elo[ind-1]
        if dif_min is None or (elo1 - elo2) < dif_min:
            combattants = {nom1, nom2}
            dif_min = (elo1 - elo2)
    return combattants

#Complexité O(NlogN)
