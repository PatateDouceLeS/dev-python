# -*-coding:Latin-1 -*-
import joueur as j

JOUEUR1 = {'id':1, 'nom':'Mathis', 'nb_pts':42}
JOUEUR2 = {'id':3, 'nom':'Jinx', 'nb_pts':12}


def test_creer_joueur():
    assert j.creer_joueur(1, 'Mathis', 42) == JOUEUR1
    assert j.creer_joueur(3, 'Jinx', 12) == JOUEUR2

def test_get_id():
    assert j.get_id(JOUEUR1) == 1
    assert j.get_id(JOUEUR2) == 3

def test_get_nom():
    assert j.get_nom(JOUEUR1) == 'Mathis'
    assert j.get_nom(JOUEUR2) == 'Jinx'

def test_get_points():
    assert j.get_points(JOUEUR1) == 42
    assert j.get_points(JOUEUR2) == 12

def test_ajouter_points():
    #C'est une modification de paramètres donc je crée des cariables pour ne pas changer les paramètrees globaux 
    joueur1 = {'id':1, 'nom':'Mathis', 'nb_pts':42}
    joueur2 = {'id':3, 'nom':'Jinx', 'nb_pts':12}
    assert j.ajouter_points(joueur1, 0) == 42
    assert j.ajouter_points(joueur2, 20) == 32


def test_id_joueur_droite():
    assert j.id_joueur_droite(JOUEUR1) == 2
    assert j.id_joueur_droite(JOUEUR2) == 4

def test_id_joueur_gauche():
    assert j.id_joueur_gauche(JOUEUR1) == 4
    assert j.id_joueur_gauche(JOUEUR2) == 2

def test_id_joueur_haut():
    assert j.id_joueur_haut(JOUEUR1) == 3
    assert j.id_joueur_haut(JOUEUR2) == 1
