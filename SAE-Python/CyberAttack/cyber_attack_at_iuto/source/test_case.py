# -*-coding:Latin-1 -*-
import case as c
import protection as p
import trojan as t
import equipement as e

TROJAN1 = t.creer_trojan(2, 1, 'B')
TROJAN2 = t.creer_trojan(2, 2, 'B')
TROJAN3 = t.creer_trojan(3, 4, 'B')
TROJAN4 = t.creer_trojan(4, 1, 'H')
PROTECTION1 = p.creer_protection(3, 3)
PROTECTION2 = p.creer_protection(5, 0)
EQUIPEMENT1 = e.creer_equipement(0,4)
CASE1 = {'flèche':'B', 'protec':PROTECTION1, 'serv':None, 'troj_presents':[TROJAN1, TROJAN2, TROJAN3], 'troj_entrants':[], 'avatar':False}
CASE2 = {'flèche':'', 'protec':PROTECTION2, 'serv':EQUIPEMENT1, 'troj_presents':[], 'troj_entrants':[], 'avatar':True}


def test_creer_case():
    assert c.creer_case('B', PROTECTION1, None, [TROJAN1, TROJAN2, TROJAN3], False) == CASE1
    assert c.creer_case('', PROTECTION2, EQUIPEMENT1, [], True) == CASE2


def test_get_fleche():
    assert c.get_fleche(CASE1) == 'B'
    assert c.get_fleche(CASE2) == ''


def test_get_protection():
    assert c.get_protection(CASE1) == PROTECTION1
    assert c.get_protection(CASE2) == PROTECTION2


def test_get_serveur():
    assert c.get_serveur(CASE1) == None
    assert c.get_serveur(CASE2) == EQUIPEMENT1


def test_get_trojans_pres():
    assert c.get_trojans_pres(CASE1) == [TROJAN1, TROJAN2, TROJAN3]
    assert c.get_trojans_pres(CASE2) == []


def test_get_trojans_entrants():
    assert c.get_trojans_entrants(CASE1) == []
    assert c.get_trojans_entrants(CASE2) == []


def test_set_fleche():
    case1 = CASE1.copy()
    case2 = CASE2.copy()
    c.set_fleche(case1, 'H')
    c.set_fleche(case2, 'B')
    assert c.get_fleche(case1) == 'H'
    assert c.get_fleche(case2) == 'B'


def test_set_serveur():
    case1 = CASE1.copy()
    case2 = CASE2.copy()
    c.set_serveur(case1, EQUIPEMENT1)
    c.set_serveur(case2, None)
    assert c.get_serveur(case1) == EQUIPEMENT1
    assert c.get_serveur(case2) == None


def test_set_protection():
    case1 = CASE1.copy()
    case2 = CASE2.copy()
    c.set_protection(case1, PROTECTION2)
    c.set_protection(case2, PROTECTION1)
    assert c.get_protection(case1) == PROTECTION2
    assert c.get_protection(case2) == PROTECTION1


def test_set_les_trojans():
    case1 = CASE1.copy()
    case2 = CASE2.copy()
    c.set_les_trojans(case1, [TROJAN1], [TROJAN1, TROJAN3])
    c.set_les_trojans(case2, [TROJAN2, TROJAN3], [TROJAN4])
    assert c.get_trojans_pres(case1) == [TROJAN1]
    assert c.get_trojans_entrants(case1) == [TROJAN1, TROJAN3]
    assert c.get_trojans_pres(case2) == [TROJAN2, TROJAN3]
    assert c.get_trojans_entrants(case2) == [TROJAN4]


def test_ajouter_trojan():
    case1 = CASE1.copy()
    case2 = CASE2.copy()
    c.ajouter_trojan(case1, TROJAN4)
    c.ajouter_trojan(case1, TROJAN3)
    c.ajouter_trojan(case2, TROJAN1)
    assert c.get_trojans_entrants(case1) == [TROJAN4, TROJAN3]
    assert c.get_trojans_entrants(case2) == [TROJAN1]


def test_reinit_trojans_entrants():
    case1 = CASE1.copy()
    case2 = CASE2.copy()
    c.ajouter_trojan(case1, TROJAN4)
    c.ajouter_trojan(case1, TROJAN3)
    c.ajouter_trojan(case2, TROJAN1)

    c.reinit_trojans_entrants(case1)
    c.reinit_trojans_entrants(case2)
    assert c.get_trojans_entrants(case1) == []
    assert c.get_trojans_entrants(case2) == []


def test_mettre_a_jour_case():
    case1 = CASE1.copy()
    case2 = CASE2.copy()
    assert c.mettre_a_jour_case(case1) == {2:2, 3:1}
    assert c.mettre_a_jour_case(case2) == dict()


def test_poser_avatar():
    case1 = CASE1.copy()
    case2 = CASE2.copy()
    assert c.poser_avatar(case1) == {2:2, 3:1}
    assert c.poser_avatar(case2) == dict()


def test_contient_avatar():
    case1 = CASE1.copy()
    case2 = CASE2.copy()
    assert not c.contient_avatar(case1)
    assert c.contient_avatar(case2)


def test_enlever_avatar():
    case1 = CASE1.copy()
    case2 = CASE2.copy()
    c.enlever_avatar(case1)
    c.enlever_avatar(case2)
    assert not c.contient_avatar(case1)
    assert not c.contient_avatar(case2)
