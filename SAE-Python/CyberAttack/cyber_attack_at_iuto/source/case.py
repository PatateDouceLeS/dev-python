# -*-coding:Latin-1 -*-
"""
              Projet CyberAttack@IUT'O
        SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

    #Miaou
    Module case.py
    ce module gère les cases du plateau
"""
import trojan as t
import protection as p


def creer_case(fleche='', la_protection=p.creer_protection(), serveur=None, liste_trojans=[], avatar=False):
    """créer une case du plateau

    Args:
        fleche (str): une des quatre directions 'H' 'B' 'G' 'D' ou '' si pas de flèche sur la case
        la_protection (dict): l'objet de protection posé sur la case (None si pas d'objet)      #Pourquoi il y a la valeur PAS_DE_PROTECTION dans protection.py                   
        serveur (dict): le serveur posé sur la case (None si pas de serveur)                    si on doit mettre None quand il n'y a pas de protection ?
        liste_trojans (list): la liste des trojans présents sur le case
        avatar (int): True s'il y a un avatar sur la case, False sinon

    Returns:
        dict: la représentation d'une case
    """
    return {'flèche':fleche, 'protec':la_protection, 'serv':serveur, 'troj_presents':liste_trojans, 'troj_entrants':[], 'avatar':avatar}


def get_fleche(case):
    """retourne la direction de la flèche de la case

    Args:
        case (dict): une case

    Returns:
        str: la direction 'H', 'B', 'G', 'D' ou '' si pas de flèche sur la case
    """
    return case['flèche']


def get_protection(case):
    """retourne l'objet de protection qui se trouve sur la case

    Args:
        case (dict): une case

    Returns:
        dict: l'objet de protection présent sur la case (None si pas d'objet)
    """
    return case['protec']


def get_serveur(case):
    """retourne le serveur qui se trouve sur la case

    Args:
        case (dict): une case

    Returns:
        dict: le serveur présent sur la case (None si pas d'objet)
    """
    return case['serv']


def get_trojans_pres(case):
    """retourne la liste des trojans présents sur la case

    Args:
        case (dict): une case

    Returns:
        list: la liste des trojans présents sur la case
    """
    return case['troj_presents']


def get_trojans_entrants(case):
    """retourne la liste des trojans qui vont arriver sur la case

    Args:
        case (dict): une case

    Returns:
        list: la liste des trojans qui vont arriver sur la case
    """
    return case['troj_entrants']


def set_fleche(case, direction):
    """affecte une direction à la case

    Args:
        case (dict): une case
        direction (dict): 'H', 'B', 'G', 'D' ou '' si pas de flèche sur la case
    """
    case['flèche'] = direction


def set_serveur(case, serveur):
    """affecte un serveur à la case

    Args:
        case (dict): une case
        serveur (str): le serveur
    """
    case['serv'] = serveur


def set_protection(case, la_protection):
    """affecte une protection à la case

    Args:
        case (dict): une case
        la_protection (dict): la protection
    """
    case['protec'] = la_protection


def set_les_trojans(case, trojans_presents, trojans_entrants):
    """fixe la liste des trojans présents et les trojans arrivant sur la case 

    Args:
        case (dict): une case
        trojans_presents (list): une liste de trojans
        trojans_entrants (list): une liste de trojans
    """
    case['troj_presents'] = trojans_presents
    case['troj_entrants'] = trojans_entrants


def ajouter_trojan(case, un_trojan):
    """ajouter un nouveau trojan arrivant à une case

    Args:
        case (dict): la case
        un_trojan (dict): le trojan à ajouter
    """
    case['troj_entrants'].append(un_trojan)


def reinit_trojans_entrants(case):
    """reinitialise la liste des trojans entrants à la liste vide

    Args:
        case (dict): une case
    """
    case['troj_entrants'] = []


def mettre_a_jour_case(case):
    """met les trojans arrivants comme présents et réinitialise les trojans arrivants
       change la direction du trojan si nécessaire (si la case comporte une flèche)
       la fonction retourne un dictionnaire qui indique pour chaque numéro de joueur
       le nombre de trojans qui vient d'arriver sur la case.
       La fonction enlève une resistance à protection qui se trouve sur elle et la détruit si
       la protection est arrivée à 0.

    Args:
        case (dict): la case

    Returns:
        dict: un dictionnaire dont les clés sont les numéros de joueur et les valeurs
              le nombre de trojans arrivés sur la case pour ce joueur
    """
    dico_troj = dict()
    dir_fleche = get_fleche(case)
    protection = get_protection(case)
    for troj in get_trojans_pres(case):
        #Direction trojans
        if dir_fleche != '': 
            t.set_direction(troj, dir_fleche)
        #Dictionnaire des trojans
        createur_troj = t.get_createur(troj)
        if createur_troj not in dico_troj:
            dico_troj[createur_troj] = 0
        dico_troj[createur_troj] += 1
        #Enleve 1 resistance à la protection
        if p.get_type_p(protection) != p.PAS_DE_PROTECTION:
            p.enlever_resistance(protection)
            if p.get_resistance(protection) <= 0:
                p.definir_type(protection, p.PAS_DE_PROTECTION)
    #Met à jour trojans entrants et présent
    set_les_trojans(case, get_trojans_entrants(case), reinit_trojans_entrants(case))
    return dico_troj
    

def poser_avatar(case):
    """pose l'avatar sur cette case et élimines les trojans présents sur la case.
       la fonction indique combien de trojans ont été éliminés

    Args:
        case (dict): une case

    Returns:
        dict: un dictionnaire dont les clés sont les numéros de joueur et les valeurs le nombre
        de trojans éliminés pour ce joueur.
    """
    case['avatar'] = True
    dico_avatar = dict()
    for troj in get_trojans_pres(case):
        createur_troj = t.get_createur(troj)
        if createur_troj not in dico_avatar:
            dico_avatar[createur_troj] = 0
        dico_avatar[createur_troj] += 1
    set_les_trojans(case, [], get_trojans_entrants(case))
    return dico_avatar


def contient_avatar(case):
    """vérifie si l'avatar se trouve sur la case

    Args:
        case (dict): une case

    Returns:
        bool: True si l'avatar est sur la case et False sinon False
    """
    return case['avatar']


def enlever_avatar(case):
    """enlève l'avatar de la case

    Args:
        case (dict): une case
    """
    case['avatar'] = False
