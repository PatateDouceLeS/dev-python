"""
             Projet CyberAttack@IUT'O
        SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

    Module matrice.py
    Ce module de gestion des matrices
"""


def creer_matrice(nb_lig, nb_col, val_defaut=None):
    """créer une matrice contenant nb_lig lignes et nb_col colonnes avec
       pour valeur par défaut val_defaut

    Args:
        nb_lig (int): un entier strictement positif
        nb_col (int): un entier strictement positif
        val_defaut (Any, optional): La valeur par défaut des éléments de la matrice.
                                    Defaults to None.
    Returns:
        dict: la matrice
    """
    matrice = dict()
    for ligne in range(nb_lig):
        for colonne in range(nb_col):
            matrice[(ligne, colonne)] = val_defaut
    return matrice



def get_nb_lignes(matrice):
    """retourne le nombre de lignes de la matrice

    Args:
        matrice (dict): une matrice

    Returns:
        int: le nombre de lignes de la matrice
    """
    max_li = None
    for (ligne, _) in matrice:
        if max_li is None or ligne > max_li:
            max_li = ligne
    return max_li+1



def get_nb_colonnes(matrice):
    """retourne le nombre de colonnes de la matrice

    Args:
        matrice (dict): une matrice

    Returns:
        int: le nombre de colonnes de la matrice
    """
    max_col = None
    for (_, colonne) in matrice:
        if max_col is None or colonne > max_col:
            max_col = colonne
    return max_col+1



def get_val(matrice, lig, col):
    """retourne la valeur en lig, col de la matrice

    Args:
        matrice (dict): une matrice
        lig (int): numéro de la ligne (en commençant par 0)
        col (int): numéro de la colonne (en commençant par 0)

    Returns:
        Any: la valeur en lig, col de la matrice
    """
    return matrice[(lig, col)]


def set_val(matrice, lig, col, val):
    """stocke la valeur val en lig, col de la matrice

    Args:
        matrice (dict): une matrice
        lig (int): numéro de la ligne (en commençant par 0)
        col (int): numéro de la colonne (en commençant par 0)
        val (Any): la valeur à stocker
    """
    matrice[(lig, col)] = def set_val(matrice, lig, col, val):



def max_matrice(matrice, interdits=None):
    """retourne la liste des coordonnées des case contenant la valeur la plus grande de la matrice
        Ces case ne doivent pas être parmi les interdits

    Args:
        matrice (dict): une matrice
        interdits (set): un ensemble de tuples (ligne,colonne) de case interdites. Defaults to None

    Returns:
        tuple: les coordonnées de case de valeur maximale dans la matrice (hors cases interdites)
    """
    position_max = None
    for (position, valeur) in matrice.items():
        if position not in interdits and (position_max is None or valeur > val_max):
            val_max = valeur
            position_max = position
    return position_max


DICO_DIR = {(-1, -1): 'HD', (-1, 0): 'HH', (-1, 1): 'HD', (0, -1): 'GG',
            (0, 1): 'DD', (1, -1): 'BG', (1, 0): 'BB', (1, 1): 'BD', (0, 0): 'BB'}


def direction_max_voisin(matrice, ligne, colonne):
    """retourne la liste des directions qui permette d'aller vers la case voisine de (ligne,colonne)
       la plus grande avec en plus la direction qui permet de se rapprocher du milieu de la matrice
       si ligne,colonne n'est pas le milieu de la matrice

    Args:
        matrice (dict): une matrice
        ligne (int): le numéro de la ligne de la case considérée
        colonne (int): le numéro de la colonne de la case considérée

    Returns:
        str: deux lettres indiquant la direction DD -> droite , HD -> Haut Droite,
                                                 HH -> Haut, HG -> Haut gauche,
                                                 GG -> Gauche, BG -> Bas Gauche, BB -> Bas
    """
    pass
