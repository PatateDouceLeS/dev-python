# -*-coding:Latin-1 -*-
import equipement as eq
 
def test_creer_equipement():
    assert eq.creer_equipement('', None) == {'type':'', 'resist':None}
    assert eq.creer_equipement('Serveur', 10) == {'type':'Serveur', 'resist':10}
    assert eq.creer_equipement('Ordinateur', 5) == {'type':'Ordinateur', 'resist':5}


def test_attaque():
    serveur = {'type':'Serveur', 'resist':10}
    ordi = {'type':'Ordinateur', 'resist':0}
    eq.attaque(serveur)
    eq.attaque(ordi)
    assert serveur == {'type':'Serveur', 'resist':9}
    assert ordi == {'type':'Ordinateur', 'resist':-1}
    
def test_est_detruit():
    assert not eq.est_detruit({'type':'Serveur', 'resist':10})
    assert eq.est_detruit({'type':'Ordinateur', 'resist':0})
    assert eq.est_detruit({'type':'Ordinateur', 'resist':-1})

def test_get_resistance():
    assert eq.get_resistance({'type':'Serveur', 'resist':10}) == 10
    assert eq.get_resistance({'type':'Ordinateur', 'resist':0}) == 0

def test_get_type():
    assert eq.get_type({'type':'Serveur', 'resist':10}) == 'Serveur'
    assert eq.get_type({'type':'Ordinateur', 'resist':0}) == 'Ordinateur'

def test_set_resistance():
    serveur = {'type':'Serveur', 'resist':10}
    ordi = {'type':'Ordinateur', 'resist':0}
    eq.set_resistance(serveur, 5)
    eq.set_resistance(ordi, 2)
    assert serveur == {'type':'Serveur', 'resist':5}
    assert ordi == {'type':'Ordinateur', 'resist':2}

