# -*-coding:Latin-1 -*-
"""
              Projet CyberAttack@IUT'O
        SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

    Module protection.py
    gère les protections que les joueurs peuvent utiliser
"""

DPO = 0
FIREWALL_BDHG = 1
FIREWALL_BGHD = 2
DONNEES_PERSONNELLES = 3
ANTIVIRUS = 4
PAS_DE_PROTECTION = 5
RESISTANCE = 2


def creer_protection(type_p=5, resistance=0):
    """créer une protection

    Args:
        type_p (int): type de la protection
        resistance (int): nombre d'attaques que peut supporter la protection

    Returns:
        dict: une protection
    """
    return {'type':type_p, 'resist':resistance}


def get_type_p(protection):
    """retourne le type de la protection

    Args:
        protection (dict): une protection

    Returns:
        int: le type de la protection
    """
    return protection['type']


def get_resistance(protection):
    """retourne la résistance de la protection

    Args:
        protection (dict): une protection

    Returns:
        int: la resistance de la protection
    """
    return protection['resist']


def enlever_resistance(protection):
    """Enlève un point de résistance de la protection et retourne la resistance restante

    Args:
        protection (dict): une protection

    Returns:
        int: la resistance restante
    """
    protection['resist'] += -1
    return protection['resist']

def definir_type(protection, type):
    """Définit le type de la protection

    Args:
        protection (dict): une protection
    """
    protection['type'] = type

def definir_resistance(protection, resist):
    """Définit la résistance de la protection

    Args:
        protection (dict): une protection
    """
    protection['resist'] = resist