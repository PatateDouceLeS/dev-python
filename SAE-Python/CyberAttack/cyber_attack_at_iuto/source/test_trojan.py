# -*-coding:Latin-1 -*-
from equipement import get_type
import trojan as t

TROJAN_TEST1 = {'createur':3, 'type':1, 'direct':'H'}
TROJAN_TEST2 = {'createur':1, 'type':0, 'direct':'D'}

def test_creer_trojan():
    assert t.creer_trojan(3, 1, 'H') == TROJAN_TEST1
    assert t.creer_trojan(1, 0, 'D') == TROJAN_TEST2

def test_get_createur():
    assert t.get_createur(TROJAN_TEST1) == 3
    assert t.get_createur(TROJAN_TEST2) == 1

def test_get_type():
    assert t.get_type(TROJAN_TEST1) == 1
    assert t.get_type(TROJAN_TEST2) == 0

def test_get_direction():
    assert t.get_direction(TROJAN_TEST1) == 'H'
    assert t.get_direction(TROJAN_TEST2) == 'D'

def test_set_createur():
    trojan1 = {'createur':3, 'type':1, 'direct':'H'}
    trojan2 = {'createur':1, 'type':0, 'direct':'D'}
    t.set_createur(trojan1, 1)
    t.set_createur(trojan2, 2)
    assert trojan1 == {'createur':1, 'type':1, 'direct':'H'}
    assert trojan2 == {'createur':2, 'type':0, 'direct':'D'}

def test_set_direction():
    trojan1 = {'createur':3, 'type':1, 'direct':'H'}
    trojan2 = {'createur':1, 'type':0, 'direct':'D'}
    t.set_direction(trojan1, 'D')
    t.set_direction(trojan2, 'G')
    assert trojan1 == {'createur':3, 'type':1, 'direct':'D'}
    assert trojan2 == {'createur':1, 'type':0, 'direct':'G'}

def test_set_type():
    trojan1 = {'createur':3, 'type':1, 'direct':'H'}
    trojan2 = {'createur':1, 'type':0, 'direct':'D'}
    t.set_type(trojan1, 4)
    t.set_type(trojan2, 3)
    assert trojan1 == {'createur':3, 'type':4, 'direct':'H'}
    assert trojan2 == {'createur':1, 'type':3, 'direct':'D'}

def test_inverser_direction():
    trojan1 = {'createur':3, 'type':1, 'direct':'H'}
    trojan2 = {'createur':1, 'type':0, 'direct':'D'}
    t.inverser_direction(trojan1)
    t.inverser_direction(trojan2)
    assert trojan1 == {'createur':3, 'type':1, 'direct':'B'}
    assert trojan2 == {'createur':1, 'type':0, 'direct':'G'}

def test_changer_direction_angle_bdhg():
    trojan1 = {'createur':3, 'type':1, 'direct':'H'}
    trojan2 = {'createur':1, 'type':0, 'direct':'D'}
    t.changer_direction_angle_bdhg(trojan1)
    t.changer_direction_angle_bdhg(trojan2)
    assert trojan1 == {'createur':3, 'type':1, 'direct':'D'}
    assert trojan2 == {'createur':1, 'type':0, 'direct':'B'}

def test_changer_direction_angle_bghd():
    trojan1 = {'createur':3, 'type':1, 'direct':'H'}
    trojan2 = {'createur':1, 'type':0, 'direct':'D'}
    t.changer_direction_angle_bghd(trojan1)
    t.changer_direction_angle_bghd(trojan2)
    assert trojan1 == {'createur':3, 'type':1, 'direct':'G'}
    assert trojan2 == {'createur':1, 'type':0, 'direct':'H'}
