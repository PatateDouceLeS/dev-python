# -*-coding:Latin-1 -*-
import protection as p

PROTECTION1 = {'type':p.FIREWALL_BDHG, 'resist':p.RESISTANCE}
PROTECTION2 = {'type':p.DONNEES_PERSONNELLES, 'resist':3}

def test_creer_protection():
    assert p.creer_protection(1, p.RESISTANCE) == PROTECTION1
    assert p.creer_protection(3, 3) == PROTECTION2

def test_get_type():
    assert p.get_type_p(PROTECTION1) == p.FIREWALL_BDHG
    assert p.get_type_p(PROTECTION2) == p.DONNEES_PERSONNELLES

def test_get_resistance():
    assert p.get_resistance(PROTECTION1) == p.RESISTANCE
    assert p.get_resistance(PROTECTION2) == 3

def test_enlever_resistance():
    protec1 = PROTECTION1.copy()
    protec2 = PROTECTION2.copy()
    assert p.enlever_resistance(protec1) == p.RESISTANCE - 1
    assert p.enlever_resistance(protec2) == 2

test_enlever_resistance()

def test_definir_type():
    protec1 = PROTECTION1.copy()
    protec2 = PROTECTION2.copy()
    p.definir_type(protec1, p.ANTIVIRUS)
    p.definir_type(protec2, p.FIREWALL_BGHD)
    assert p.get_type_p(protec1) == p.ANTIVIRUS
    assert p.get_type_p(protec2) == p.FIREWALL_BGHD

def test_definir_resistance():
    protec1 = PROTECTION1.copy()
    protec2 = PROTECTION2.copy()
    p.definir_resistance(protec1, 2)
    p.definir_resistance(protec2, 0)
    assert p.get_resistance(protec1) == 2
    assert p.get_resistance(protec2) == 0