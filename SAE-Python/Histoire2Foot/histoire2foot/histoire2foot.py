# -*-coding:Latin-1 -*-
"""Fichier source de la SAE 1.01 partie 1
Historique des matchs de football internationaux
"""

# ---------------------------------------------------------------------------------------------
# Exemples de données pour vous aidez à faire des tests
# ---------------------------------------------------------------------------------------------
    
# exemples de matchs de foot
MATCH1 = ('2021-06-28', 'France', 'Switzerland', 3, 3, 'UEFA Euro', 'Bucharest', 'Romania', True)
MATCH2 = ('1998-07-12', 'France', 'Brazil', 3, 0, 'FIFA World Cup', 'Saint-Denis', 'France', False)
MATCH3 = ('1978-04-05', 'Germany', 'Brazil', 0, 1, 'Friendly', 'Hamburg', 'Germany', False)

#exemples de liste de matchs de foot
LISTE1 = [('1970-04-08', 'France', 'Bulgaria', 1, 1, 'Friendly', 'Rouen', 'France', False),
        ('1970-04-28', 'France', 'Romania', 2, 0, 'Friendly', 'Reims', 'France', False),
        ('1970-09-05', 'France', 'Czechoslovakia', 3, 0, 'Friendly', 'Nice', 'France', False),
        ('1970-11-11', 'France', 'Norway', 3, 1, 'UEFA Euro qualification', 'Lyon', 'France', False)
        ]
LISTE2 = [('1901-03-09', 'England', 'Northern Ireland', 3, 0, 'British Championship', 'Southampton', 'England', False),
        ('1901-03-18', 'England', 'Wales', 6, 0, 'British Championship', 'Newcastle', 'England', False),
        ('1901-03-30', 'England', 'Scotland', 2, 2, 'British Championship', 'London', 'England', False),
        ('1902-05-03', 'England', 'Scotland', 2, 2, 'British Championship', 'Birmingham', 'England', False),
        ('1903-02-14', 'England', 'Northern Ireland', 4, 0, 'British Championship', 'Wolverhampton', 'England', False),
        ('1903-03-02', 'England', 'Wales', 2, 1, 'British Championship', 'Portsmouth', 'England', False),
        ('1903-04-04', 'England', 'Scotland', 1, 2, 'British Championship', 'Sheffield', 'England', False),
        ('1905-02-25', 'England', 'Northern Ireland', 1, 1, 'British Championship', 'Middlesbrough', 'England', False),
        ('1905-03-27', 'England', 'Wales', 3, 1, 'British Championship', 'Liverpool', 'England', False),
        ('1905-04-01', 'England', 'Scotland', 1, 0, 'British Championship', 'London', 'England', False),
        ('1907-02-16', 'England', 'Northern Ireland', 1, 0, 'British Championship', 'Liverpool', 'England', False),
        ('1907-03-18', 'England', 'Wales', 1, 1, 'British Championship', 'London', 'England', False),
        ('1907-04-06', 'England', 'Scotland', 1, 1, 'British Championship', 'Newcastle', 'England', False),
        ('1909-02-13', 'England', 'Northern Ireland', 4, 0, 'British Championship', 'Bradford', 'England', False),
        ('1909-03-15', 'England', 'Wales', 2, 0, 'British Championship', 'Nottingham', 'England', False),
        ('1909-04-03', 'England', 'Scotland', 2, 0, 'British Championship', 'London', 'England', False)
        ]
LISTE3 = [('1901-03-30', 'Belgium', 'France', 1, 2, 'Friendly', 'Bruxelles', 'Belgium', False),
        ('1901-03-30', 'England', 'Scotland', 2, 2, 'British Championship', 'London', 'England', False),
        ('1903-04-04', 'Brazil', 'Argentina', 3, 0, 'Friendly', 'Sao Paulo', 'Brazil', False),
        ('1903-04-04', 'England', 'Scotland', 1, 2, 'British Championship', 'Sheffield', 'England', False),
        ('1970-09-05', 'France', 'Czechoslovakia', 3, 0, 'Friendly', 'Nice', 'France', False),
        ('1970-11-11', 'France', 'Norway', 3, 1, 'UEFA Euro qualification', 'Lyon', 'France', False)
        ]
LISTE4 = [('1978-03-19', 'Argentina', 'Peru', 2, 1, 'Copa Ramón Castilla', 'Buenos Aires', 'Argentina', False),
        ('1978-03-29', 'Argentina', 'Bulgaria', 3, 1, 'Friendly', 'Buenos Aires', 'Argentina', False),
        ('1978-04-05', 'Argentina', 'Romania', 2, 0, 'Friendly', 'Buenos Aires', 'Argentina', False),
        ('1978-05-03', 'Argentina', 'Uruguay', 3, 0, 'Friendly', 'Buenos Aires', 'Argentina', False),
        ('1978-06-01', 'Germany', 'Poland', 0, 0, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True),
        ('1978-06-02', 'Argentina', 'Hungary', 2, 1, 'FIFA World Cup', 'Buenos Aires', 'Argentina', False),
        ('1978-06-02', 'France', 'Italy', 1, 2, 'FIFA World Cup', 'Mar del Plata', 'Argentina', True),
        ('1978-06-02', 'Mexico', 'Tunisia', 1, 3, 'FIFA World Cup', 'Rosario', 'Argentina', True),
        ('1978-06-03', 'Austria', 'Spain', 2, 1, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True),
        ('1978-06-03', 'Brazil', 'Sweden', 1, 1, 'FIFA World Cup', 'Mar del Plata', 'Argentina', True),
        ('1978-06-03', 'Iran', 'Netherlands', 0, 3, 'FIFA World Cup', 'Mendoza', 'Argentina', True),
        ('1978-06-03', 'Peru', 'Scotland', 3, 1, 'FIFA World Cup', 'Córdoba', 'Argentina', True),
        ('1978-06-06', 'Argentina', 'France', 2, 1, 'FIFA World Cup', 'Buenos Aires', 'Argentina', False),
        ('1978-06-06', 'Germany', 'Mexico', 6, 0, 'FIFA World Cup', 'Córdoba', 'Argentina', True),
        ('1978-06-06', 'Hungary', 'Italy', 1, 3, 'FIFA World Cup', 'Mar del Plata', 'Argentina', True),
        ('1978-06-06', 'Poland', 'Tunisia', 1, 0, 'FIFA World Cup', 'Rosario', 'Argentina', True),
        ('1978-06-07', 'Austria', 'Sweden', 1, 0, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True),
        ('1978-06-07', 'Brazil', 'Spain', 0, 0, 'FIFA World Cup', 'Mar del Plata', 'Argentina', True),
        ('1978-06-07', 'Iran', 'Scotland', 1, 1, 'FIFA World Cup', 'Córdoba', 'Argentina', True),
        ('1978-06-07', 'Netherlands', 'Peru', 0, 0, 'FIFA World Cup', 'Mendoza', 'Argentina', True),
        ('1978-06-10', 'Argentina', 'Italy', 0, 1, 'FIFA World Cup', 'Buenos Aires', 'Argentina', False),
        ('1978-06-10', 'France', 'Hungary', 3, 1, 'FIFA World Cup', 'Mar del Plata', 'Argentina', True),
        ('1978-06-10', 'Germany', 'Poland', 1, 3, 'FIFA World Cup', 'Rosario', 'Argentina', True),
        ('1978-06-11', 'Austria', 'Brazil', 0, 1, 'FIFA World Cup', 'Mar del Plata', 'Argentina', True),
        ('1978-06-11', 'Iran', 'Peru', 1, 4, 'FIFA World Cup', 'Córdoba', 'Argentina', True),
        ('1978-06-11', 'Netherlands', 'Scotland', 2, 3, 'FIFA World Cup', 'Mendoza', 'Argentina', True),
        ('1978-06-11', 'Spain', 'Sweden', 1, 0, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True),
        ('1978-06-14', 'Argentina', 'Poland', 2, 0, 'FIFA World Cup', 'Rosario', 'Argentina', False),
        ('1978-06-14', 'Austria', 'Netherlands', 1, 5, 'FIFA World Cup', 'Córdoba', 'Argentina', True),
        ('1978-06-14', 'Brazil', 'Peru', 3, 0, 'FIFA World Cup', 'Mendoza', 'Argentina', True),
        ('1978-06-14', 'Germany', 'Italy', 0, 0, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True),
        ('1978-06-18', 'Argentina', 'Brazil', 0, 0, 'FIFA World Cup', 'Rosario', 'Argentina', False),
        ('1978-06-18', 'Austria', 'Italy', 0, 1, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True),
        ('1978-06-18', 'Germany', 'Netherlands', 2, 2, 'FIFA World Cup', 'Córdoba', 'Argentina', True),
        ('1978-06-18', 'Peru', 'Poland', 0, 1, 'FIFA World Cup', 'Mendoza', 'Argentina', True),
        ('1978-06-21', 'Argentina', 'Peru', 6, 0, 'FIFA World Cup', 'Rosario', 'Argentina', False),
        ('1978-06-21', 'Austria', 'Germany', 3, 2, 'FIFA World Cup', 'Córdoba', 'Argentina', True),
        ('1978-06-21', 'Brazil', 'Poland', 3, 1, 'FIFA World Cup', 'Mendoza', 'Argentina', True),
        ('1978-06-21', 'Italy', 'Netherlands', 1, 2, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True),
        ('1978-06-24', 'Brazil', 'Italy', 2, 1, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True),
        ('1978-06-25', 'Argentina', 'Netherlands', 3, 1, 'FIFA World Cup', 'Buenos Aires', 'Argentina', False)
        ]

# --------------------------------------------------------------------------------------------------
# listes des fonctions à implémenter
# --------------------------------------------------------------------------------------------------

# Fonctions à implémenter dont les tests sont fournis


def equipe_gagnante(match):
    """retourne le nom de l'équipe qui a gagné le match. Si c'est un match nul on retourne None

    Args:
        match (tuple): un match

    Returns:
        str: le nom de l'équipe gagnante (ou None si match nul)
    """
    if len(match) == 9:
        if match[4] > match[3]:
            return match[2]
        if match[4] < match[3]:
            return match[1]
    return None



def victoire_a_domicile(match):
    """indique si le match correspond à une victoire à domicile

    Args:
        match (tuple): un match

    Returns:
        bool: True si le match ne se déroule pas en terrain neutre et que l'équipe qui reçoit a gagné
    """
    if len(match) == 9:
        return not match[8] and match[3] > match[4]



def nb_buts_marques(match):
    """indique le nombre total de buts marqués lors de ce match

    Args:
        match (tuple): un match

    Returns:
        int: le nombre de buts du match
    """
    if len(match) == 9:
        nbr_buts = match[3] + match[4]
    else:
        nbr_buts = 0
    return nbr_buts



def matchs_ville(liste_matchs, ville):
    """retourne la liste des matchs qui se sont déroulés dans une ville donnée

    Args:
        liste_matchs (list): une liste de matchs
        ville (str): le nom d'une ville

    Returns:
        list: la liste des matchs qui se sont déroulé dans la ville donnée
    """
    matchs = []
    for elem in liste_matchs:
        if elem[6] == ville:
            matchs.append(elem)
    return matchs



def nombre_moyen_buts(liste_matchs, nom_competition):
    """retourne le nombre moyen de buts marqués par match pour une compétition donnée

    Args:
        liste_matchs (list): une liste de matchs
        nom_competition (str): le nom d'une compétition

    Returns:
        float: le nombre moyen de buts par match pour la compétition
    """
    cpt = 0
    nb_buts = None
    for (_, _, _, home_score, away_score, tournament, _, _, _) in liste_matchs:
        if nb_buts is None and tournament == nom_competition:
            cpt += 1
            nb_buts = (home_score + away_score)
        else:
            if tournament == nom_competition:
                cpt += 1
                nb_buts += (home_score + away_score)
    if cpt == 0:
        return None
    return nb_buts/cpt



def est_bien_trie(liste_matchs):
    """vérifie si une liste de matchs est bien trié dans l'ordre chronologique
       puis pour les matchs se déroulant le même jour, dans l'ordre alphabétique
       des équipes locales

    Args:
        liste_matchs (list): une liste de matchs

    Returns:
        bool: True si la liste est bien triée et False sinon
    """
    prec_date = None
    prec_eq = None
    for (date, home_team, _, _, _, _, _, _, _) in liste_matchs:
        if  not prec_date is None:
            if date < prec_date:
                return False
            if date == prec_date:
                if home_team <= prec_eq:
                    return False
        prec_date = date
        prec_eq = home_team
    return True



def suppr_doub(liste_nulle):
    """Supprime les doublons d'une liste

    Args:
        liste_nulle (list): Liste d'entrée avec doublons

    Returns:
        list: Liste de sortie sans doublons
    """
    liste_bien = []
    prec = None
    for elem in liste_nulle:
        if prec is None or prec != elem:
            prec = elem
            liste_bien.append(elem)
    return liste_bien

def fusionner_matchs(liste_matchs1, liste_matchs2):
    """Fusionne deux listes de matchs triées sans doublons en une liste triée sans doublon
    sachant qu'un même match peut être présent dans les deux listes

    Args:
        liste_matchs1 (list): la première liste de matchs
        liste_matchs2 (list): la seconde liste de matchs

    Returns:
        list: la liste triée sans doublon comportant tous les matchs de liste_matchs1 et liste_matchs2
    """
    ind_l1 = 0
    ind_l2 = 0
    li_fus = []
    while ind_l1 < len(liste_matchs1) and ind_l2 < len(liste_matchs2):
        if liste_matchs1[ind_l1] < liste_matchs2[ind_l2]:
            li_fus.append(liste_matchs1[ind_l1])
            ind_l1 += 1
        else:
            li_fus.append(liste_matchs2[ind_l2])
            ind_l2 += 1
    if ind_l1 < len(liste_matchs1):
        for ind in range(ind_l1, len(liste_matchs1)):
            li_fus.append(liste_matchs1[ind])
    else:
        for ind in range(ind_l2, len(liste_matchs2)):
            li_fus.append(liste_matchs2[ind])
    li_fus = suppr_doub(li_fus)
    return li_fus



def resultats_equipe(liste_matchs, equipe):
    """donne le nombre de victoire, de matchs nuls et de défaites pour une équipe donnée

    Args:
        liste_matchs (list): une liste de matchs
        equipe (str): le nom d'une équipe (pays)

    Returns:
        tuple: un triplet d'entiers contenant le nombre de victoires, nuls et défaites de l'équipe
    """
    nbr_v = 0
    nbr_d = 0
    nbr_n = 0
    for (_, home_team, away_team, home_score, away_score, _, _, _, _) in liste_matchs:
        if home_team == equipe:
            if home_score > away_score:
                nbr_v += 1
            else:
                if home_score < away_score:
                    nbr_d += 1
                else:
                    nbr_n += 1
        if away_team == equipe:
            if home_score < away_score:
                nbr_v += 1
            else:
                if home_score > away_score:
                    nbr_d += 1
                else:
                    nbr_n += 1
    return (nbr_v, nbr_d, nbr_n)



def ajout_matchs_ecart(liste_matchs, max_ecart):
    """Fait la liste de tous les matchs contenant le plus grand écart

    Args:
        liste_matchs (list): Liste de matchs
        max_ecart (int): Ecart maximal de buts

    Returns:
        list: Liste des matchs avec le plus grand écart
    """
    big_scores = []
    for match in liste_matchs:
        if ((match[3] - match[4]) or (match[4] - match[3])) == max_ecart:
            big_scores.append(match)
    return big_scores



def plus_gros_scores(liste_matchs):
    """retourne la liste des matchs pour lesquels l'écart de buts entre le vainqueur et le perdant est le plus grand

    Args:
        liste_matchs (list): une liste de matchs

    Returns:
        list: la liste des matchs avec le plus grand écart entre vainqueur et perdant
    """
    max_ecart = None
    for (_, _, _, home_score, away_score, _, _, _, _) in liste_matchs:
        if max_ecart is None:
            if home_score > away_score:
                max_ecart = home_score - away_score
            else:
                max_ecart = away_score - home_score
        else:
            if home_score > away_score and (home_score - away_score) > max_ecart:
                max_ecart = home_score - away_score
            else:
                if (away_score - home_score) > max_ecart:
                    max_ecart = away_score - home_score
    big_scores = ajout_matchs_ecart(liste_matchs, max_ecart)
    return big_scores



def liste_des_equipes(liste_matchs):
    """retourne la liste des équipes qui ont participé aux matchs de la liste
    Attention on ne veut voir apparaitre le nom de chaque équipe qu'une seule fois

    Args:
        liste_matchs (list): une liste de matchs

    Returns:
        list: une liste de str contenant le noms des équipes ayant jouer des matchs
    """
    li_eq = []
    for (_, home_team, away_team, _, _, _, _, _, _) in liste_matchs:
        if not home_team in li_eq:
            li_eq.append(home_team)
        if not away_team in li_eq:
            li_eq.append(away_team)
    return li_eq



def premiere_victoire(liste_matchs, equipe):
    """retourne la date de la première victoire de l'equipe. Si l'equipe n'a jamais gagné de match on retourne None

    Args:
        liste_matchs (list): une liste de matchs
        equipe (str): le nom d'une équipe (pays)

    Returns:
        str: la date de la première victoire de l'equipe
    """
    for (date, home_team, away_team, home_score, away_score, _, _, _, _) in liste_matchs:
        if home_team == equipe and home_score > away_score:
            return date
        if away_team == equipe and away_score > home_score:
            return date
    return None



def nb_matchs_sans_defaites(liste_matchs, equipe):
    """retourne le plus grand nombre de matchs consécutifs sans défaite pour une equipe donnée.

    Args:
        liste_matchs (list): une liste de matchs
        equipe (str): le nom d'une équipe (pays)

    Returns:
        int: le plus grand nombre de matchs consécutifs sans défaite du pays
    """
    serie_v = 0
    serie_max = 0
    for (_, home_team, away_team, home_score, away_score, _, _, _, _) in liste_matchs:
        if home_team == equipe or away_team == equipe:
            if (home_team == equipe and home_score > away_score) or (away_team == equipe and away_score > home_score):
                serie_v += 1
            else:
                serie_v = 0
        if serie_v > serie_max:
            serie_max = serie_v
    return serie_max


 #(date, home_team, away_team, home_score, away_score, tournament, city, country, neutral)



def charger_matchs(nom_fichier):
    """charge un fichier de matchs donné au format CSV en une liste de matchs

    Args:
        nom_fichier (str): nom du fichier CSV contenant les matchs

    Returns:
        list: la liste des matchs du fichier
    """
    liste_matchs = []
    try:
        fic = open(nom_fichier, "r")
        fic.readline()
        for ligne in fic:
            ligne_decoup = ligne.split(",")
            ligne_decoup[3] = int(ligne_decoup[3])
            ligne_decoup[4] = int(ligne_decoup[4])
            if ligne_decoup[-1] == "False\n":
                ligne_decoup[-1] = False
            else:
                ligne_decoup[-1] = True
            liste_matchs.append(tuple(ligne_decoup))
        fic.close()
        return liste_matchs
    except:
        return None

def sauver_matchs(liste_matchs, nom_fichier):
    """sauvegarde dans un fichier au format CSV une liste de matchs

    Args:
        liste_matchs (list): la liste des matchs à sauvegarder
        nom_fichier (str): nom du fichier CSV

    Returns:
        None: cette fonction ne retourne rien
    """
    fic = open(nom_fichier, "w")
    fic.write("date,home_team,away_team,home_score,away_score,tournament,city,country,neutral")
    for match in liste_matchs:
        fic.write(match[0]+","+match[1]+","+match[2]+","+str(match[3])+","+str(match[4])+","+match[5]+","+match[6]+","+match[7]+","+str(match[8])+"\n")
    fic.close()



def plus_de_victoires_que_defaites(liste_matchs, equipe):
    """vérifie si une équipe donnée a obtenu plus de victoires que de défaites
    Args:
        liste_matchs (list): une liste de matchs
        equipe (str): le nom d'une équipe (pays)

    Returns:
        bool: True si l'equipe a obtenu plus de victoires que de défaites
    """
    resultats = resultats_equipe(liste_matchs, equipe)
    return resultats[0] > resultats[1]



def ajout_matchs(liste_matchs, ind_max):
    """Crée la liste des matchs "spectaculaires" à partir d'un indice max

    Args:
        liste_matchs (list): Une liste de matchs
        ind_max (int): Indice du dernier match avec le plus grand nombre de buts

    Returns:
        list: Liste des matchs "spectaculaires"
    """
    matchs_spec = []
    for match in liste_matchs:
        if nb_buts_marques(match) == nb_buts_marques(liste_matchs[ind_max]):
            matchs_spec.append(match)
    return matchs_spec

def matchs_spectaculaires(liste_matchs):
    """retourne la liste des matchs les plus spectaculaires, c'est à dire les
    matchs dont le nombre total de buts marqués est le plus grand

    Args:
        liste_matchs (list): une liste de matchs

    Returns:
        list: la liste des matchs les plus spectaculaires
    """
    max_but = None
    matchs_spec = []
    for ind in range(len(liste_matchs)):
        if max_but is None or nb_buts_marques(liste_matchs[ind]) > max_but:
            max_but = nb_buts_marques(liste_matchs[ind])
            ind_max = ind
        matchs_spec = ajout_matchs(liste_matchs, ind_max)
    return matchs_spec


def ajout_equipe(liste_matchs, liste_equipes, ind_meil_eq):
    """Crée la liste des meilleurs équipes

    Args:
        liste_matchs (list): Une liste de matchs
        ind_max (int): Indice du dernier match avec le plus grand nombre de buts

    Returns:
        list: Liste des meilleurs équipes
    """
    meil_eq = []
    for equipe in liste_equipes:
        if resultats_equipe(liste_matchs, equipe)[1] == resultats_equipe(liste_matchs, liste_equipes[ind_meil_eq])[1]:
            meil_eq.append(equipe)
    return meil_eq

def meilleures_equipes(liste_matchs):
    """retourne la liste des équipes de la liste qui ont le plus petit nombre de defaites

    Args:
        liste_matchs (list): une liste de matchs

    Returns:
        list: la liste des équipes qui ont le plus petit nombre de defaites
    """
    meil_eq = []
    nbr_def_max = None
    li_eq = liste_des_equipes(liste_matchs)
    for ind in range(len(li_eq)):
        if nbr_def_max is None or resultats_equipe(liste_matchs, li_eq[ind])[1] < nbr_def_max:
            nbr_def_max = resultats_equipe(liste_matchs, li_eq[ind])[1]
            ind_meil_eq = ind
    if len(liste_matchs) != 0:
        meil_eq = ajout_equipe(liste_matchs, li_eq, ind_meil_eq)
    return meil_eq
