import histoire2foot
BOLD = '\033[1m'
END = '\033[0m'
LISTE_TEST = histoire2foot.charger_matchs("SAE-Python/histoire2foot/histoire3.csv")

#Fonctions dédiées aux interaction
def res_equipe_tournois(liste_matchs, equipe):
    """Donne les résultat d'un équipe dans un tournois en particulier

    Args:
        liste_matchs (list): Une liste de matchs
    """
    tournois = input("Pour quelle tournois ? ")
    match_tournois = []
    for match in liste_matchs:
        if match[5] == tournois:
            match_tournois.append(match)
    resultats = histoire2foot.resultats_equipe(match_tournois, equipe)
    print(BOLD+"Votre équipe a eu "+str(resultats[0])+" victoire(s), "+str(resultats[1])+" défaite(s) et "+str(resultats[2])+" match(s) nul(s)"+END)


def res_equipe_charger(liste_matchs, equipe):
    """Donne les résultats d'une équipe à partir d'un fichier chargé.
    """
    resultats = histoire2foot.resultats_equipe(liste_matchs, equipe)
    print(BOLD+"Votre équipe a eu "+str(resultats[0])+" victoire(s), "+str(resultats[1])+" défaite(s) et "+str(resultats[2])+" match(s) nul(s)"+END)


def moy_buts_charger(liste_matchs):
    """Donne la moyenne des buts des matchs d'un fichier chargé.
    """
    moy_but = histoire2foot.nombre_moyen_buts(liste_matchs, input("De quelle compétition voulez-vous la moyenne de buts ? "))
    print(BOLD+"Durant cette compétition il y a eu en moyenne "+str(moy_but)+" marqués"+END)


def plus_gros_scores_charger(liste_matchs):
    """Donne les matchs avec les plus gros score dans un fichier chargé
    """
    print(BOLD+"Voici la liste des matchs avec les plus gros scores :")
    print(histoire2foot.plus_gros_scores(liste_matchs))
    print(END)


def matchs_spectaculaires_charger(liste_matchs):
    """Donne les matchs les plus spectaculaires (avec le plus de buts marqués au total)
    """
    print(BOLD+"Voici la liste des matchs les plus spectaculaires :")
    print(histoire2foot.matchs_spectaculaires(liste_matchs))
    print(END)


def meilleurs_equipes_charger(liste_matchs):
    """Donne les équipes ayant eu le moins de défaites
    """
    print(BOLD+"Les meilleures équipes sont :")
    print(histoire2foot.meilleures_equipes(liste_matchs))
    print(END)


def plus_de_victoires_que_defaites_charger(liste_matchs, equipe):
    """Indique si une équipe a plus de victoires ou de défaites
    """
    résultat = histoire2foot.plus_de_victoires_que_defaites(liste_matchs, equipe)
    if résultat:
        print(BOLD+"Votre équipe a eu plus de victoire que de victoires !!"+END)
    else:
        print(BOLD+"Votre équipe a eu plus de victoire que de défaites..."+END)


#Programme principal
def programme_principal():
    """Répond aux demandes de l'utilisateur en fonction de ce qu'il aura choisi.
    """
    equipe = input("Quelle équipe voulez-vous suivre ? ")
    liste_matchs = histoire2foot.charger_matchs("SAE-Python/histoire2foot/"+input("Indiquez votre fichier à charger, n'oubliez pas d'indiquer l'extension de fichier. "))
    while True:
        #Pour une meilleure clarté du code, j'ai préféré faire plein de print plutôt q'un seul centenant des "\n" ou avec un for qui ferait une ligne en plus pour pas grand choses
        print("\nMENU DES OPTIONS\n")
        print("Voici les options disponibles, indiquez ce que vous voulez par le numéro de l'option (/!\\ Les options 5 et 6 peuvent prendre du temps pour les gros fichiers, surtout l'option 5 !!!")
        print("1 -> Connaitre les résultats de votre équipe pour un compétition demandée")
        print("2 -> Connaitre les résultats de votre équipe depuis un fichier qu'on aura chargé")
        print("3 -> Donne le nombre moyen de buts marqués durant la compétition chargée")
        print("4 -> Donne la liste des matchs avec la plus grande différence de points")
        print("5 -> Donne les matchs les plus spectaculaires (avec le plus de buts marqués au total)")
        print("6 -> Donne les équipes ayant eu le moins de défaites dans la liste de matchs")
        print("7 -> Indique si votre équipe a plus de victoires ou de défaites")
        print("Ecrivez 'Quitter' pr quitter le programmme.")
        choix = input("Quelle option souhaitez-vous ? ")
        if choix == "1":
            res_equipe_tournois(liste_matchs, equipe)
        if choix == "2":
            res_equipe_charger(liste_matchs, equipe)
        if choix == "3":
            moy_buts_charger(liste_matchs)
        if choix == "4":
            plus_gros_scores_charger(liste_matchs)
        if choix == "5":
            matchs_spectaculaires_charger(liste_matchs)
        if choix == "6":
            meilleurs_equipes_charger(liste_matchs)
        if choix == "7":
            plus_de_victoires_que_defaites_charger(liste_matchs, equipe)
        if choix == "Quitter" or choix == "quitter" or choix == "QUITTER":
            break
    print(BOLD+"Vous quittez le programme, au revoir !"+END)

programme_principal()
