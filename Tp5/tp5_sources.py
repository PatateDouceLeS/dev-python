"""
Mathis PERRIN
BUT Info
TP5
"""

#Exercice 1
def cpt_valeur_liste(liste, valeur):
    """Retourne au bout de cb de valeurs on a trouvé 4 fois la valeur ou "None" le cas échéant

    Args:
        liste (list): Liste de nombres
        valeur (int): Un nombre quelconque

    Returns:
        int: None ou au bout de combien d'éléments on trouve 4 fois la valeur
    """
    cpt = 0 #le nombre d'éléments énumérés
    cpt_val = 0 #le nombre d'éléments correspondant à la valeur
    for ind in range(len(liste)):
        if liste[ind] == valeur:
            cpt_val += 1
            if cpt_val > 3:
                return cpt
        cpt += 1
    return None

def test_cpt_valeur_liste():
    """[summary]
    """
    assert cpt_valeur_liste([12, 5, 8, 48, 12, 418, 185, 17, 5, 87], 20) is None
    assert cpt_valeur_liste([4, 4, 3, 4, 4], 4) == 4
    assert cpt_valeur_liste([], 0) is None
    assert cpt_valeur_liste([45, 7, 75, 7, 7, 84, 74], 0) is None

#Exercice 2.1
def rech_chiffre(phrase):
    """Donne l'indice du premier chiffre rencontré

    Args:
        phrase (str): Une phrase quelconque

    Returns:
        int: Indice du premier chiffre obtenu
    """
    for ind in range(len(phrase)):
        if not phrase[ind].isalpha() and phrase[ind] != ' ':
            return ind
    return None

def test_rech_chiffre():
    """[summary]
    """
    assert rech_chiffre("") is None
    assert rech_chiffre("Bonjour") is None
    assert rech_chiffre("Tu as 18 ans maintenant") == 6
    assert rech_chiffre("1") == 0

#Exercice 2.2
def rech_pop(liste_villes, populations, nom_ville):
    """Reçois le nom d'une ville et en donne la pop

    Args:
        liste_villes (list): liste de villes
        populations (list): liste de population correspondant aux villes
        nom_ville (str): nom d'une ville dont on recherche le nombre d'habitant

    Returns:
        int: Pop de la ville recherché ou None
    """
    for ind in range(len(liste_villes)):
        if liste_villes[ind] == nom_ville:
            return populations[ind]
    return None

def test_rech_pop():
    assert rech_pop ([], [], "Bourges") is None
    assert rech_pop (["Blois", "Bourges", "Chartres", "Châteauroux", "Dreux", "Joué-lès-Tours", "Olivet", "Orléans", "Tours", "Vierzon"], [45871, 64668, 38426, 43442, 30664, 38250, 22168, 116238, 136463, 25725], "Vierzon") == 25725

#Exercice 3.1
def croissant(liste):
    """Indique si la liste est rangé dans l'ordre croissant par un booléen

    Args:
        liste (list): liste de valeurs quelconques

    Returns:
        bool: booléen indiquant True ou False
    """
    prec = None
    for ind in range(len(liste)):
        if ind == 0:
            prec = liste[ind]
        else:
            if liste[ind] < prec:
                return False
        prec = liste[ind]
    return True

def test_croissant():
    assert croissant([])
    assert croissant([4])
    assert croissant([-1, 8, 9, 9, 10])
    assert not croissant([2, 4, 3, 4, 5, 6])

#Exercice 3.2
def seuil(liste,val):
    """Indique si la somme des éléments d'une liste est supérieur à une valeur

    Args:
        liste (list): Une liste de valeurs quelconque
        val (int): Le seuill imposé pour la somme

    Returns:
        bool: Renvoie si le seuill est dépassé ou pas
    """
    som = None
    for elem in liste:
        if som is None:
            som = elem
        else :
            som += elem
            if som > val:
                return True
    return False

def test_seuil():
    """[summary]
    """
    assert not seuil ([], 0)
    assert not seuil ([-4], 0)
    assert seuil ([4, 2, 1, 8], 10)
    assert not seuil ([5, 4, 8, 7, 9], 100)

#Exercice 3.3

def espaces(adresse):
    """Recherche les espaces

    Args:
        adresse (str): une potentiel adresse mail

    Returns:
        bool: Indique si l'adresse contient un espace
    """
    for elem in adresse:
        if elem == " ":
            return False
    return True

def test_espaces():
    """[summary]
    """
    assert espaces("")
    assert not espaces(" ")
    assert espaces("mathis.perrin@etu.univ-orleans.fr")
    assert not espaces("math is.perri n@etu.univ -orleans.fr")


def arobase(adresse):
    """Recherche les arobase

    Args:
        adresse (str): une potentiel adresse mail

    Returns:
        ind: indice du seul arobase
    """
    aro = False
    ind_aro = None
    for ind in range(len(adresse)):
        if not aro:
            if adresse[ind] == "@":
                aro = True
                ind_aro = ind
        else :
            if adresse[ind] == "@":
                ind_aro = None
    return ind_aro

def test_arobase():
    """[summary]
    """
    assert arobase("") is None
    assert arobase("@") == 0
    assert arobase("mathis.perrin@etu.univ-orleans.fr") == 13
    assert arobase("mathis.perrin@etu.univ@orleans.fr") is None


def point(adresse,ind_aro):
    """Indique s'il y a un points après l'arobase

    Args:
        adresse (str): une adresse à vérifier

    Returns:
        bool: dis s'il y a un pts ou pas avec l'arobase
    """
    for ind in range(ind_aro,len(adresse)):
        if adresse[ind] == ".":
            return True
    return False

def test_point():
    """[summary]
    """
    assert not point("", 0) 
    assert not point("@", 0)
    assert point("mathis.perrin@etu.univ-orleans.fr", 13)
    assert not point("mathis.perrin@etuuniv-orleansfr", 13)


def mail(adresse):
    """Inque si une adresse mail est valable

    Args:
        adresse (str): une adresse mail à vérifier

    Returns:
        bool: retourne si l'adresse est bien écrite ou pas
    """
    if len(adresse) != 0 and adresse[0] != "@" and adresse[-1] != ".":
        if espaces(adresse):
            ind_aro = arobase(adresse)
            if ind_aro != None:
                if point(adresse, ind_aro):
                    return True
    return False

def test_mail():
    """[summary]
    """
    assert not mail("")
    assert mail("mathis.perrin@etu.univ-orleans.fr")
    assert not mail("mathis.perrin@etu.univ-orleans.fr.")
    assert not mail("mathi s.perrin@ etu.un iv-orleans.fr")
    assert not mail("mathis.perrin@@etu.univ-orleans.fr")
    assert not mail("mathis.perrin@etuuniv-orleansfr")


#Exercice 4.1
def max_scores(scores, joueurs, cible):
    """Donne le score maximal qu'on joueur à fait

    Args:
        scores (list): Liste des scores
        joueurs (list): Liste des joueurs
        cible (str): Le nom du joueurs dont on veut savoir le score max

    Returns:
        int: Score maximal du joueurs en question
    """
    score_max = None
    for ind in range(len(joueurs)):
        if joueurs[ind] == cible:
            if score_max is None:
                score_max = scores[ind]
            else:
                if scores[ind] > score_max:
                    score_max = scores[ind]
    return score_max

def test_max_scores():
    """[summary]
    """
    assert max_scores([], [], "") is None
    assert max_scores([352100, 325410, 312785, 220199, 127853], ['Batman', 'Robin', 'Batman', 'Joker', 'Batman'], "Batman") == 352100
    assert max_scores([352100, 325410, 312785, 220199, 127853], ['Batman', 'Robin', 'Batman', 'Joker', 'Batman'], "Joker") == 220199
    assert max_scores([352100, 325410, 312785, 220199, 127853], ['Batman', 'Robin', 'Batman', 'Joker', 'Batman'], "Le Pingouin") is None

#Exercice 4.2
def verif_tri(scores):
    """Vérifie si les score sont bien dans un ordre décroissant

    Args:
        scores (list): Une liste de scores

    Returns:
        bool: indique si la liste est bien rangée ou pas
    """
    prec = None
    for elem in scores:
        if prec is None:
            prec = elem
        else:
            if prec < elem:
                return False
    return True

def test_vetif_tri():
    """[summary]
    """
    assert verif_tri([])
    assert verif_tri([352100, 325410, 312785, 220199, 127853])
    assert verif_tri([352100, 325410, 352100, 220199, 127853])

#Exercice 4.3
def top_players(joueurs, cible):
    """Indique le nombre de fois qu'un joueurs apparait dans les scores

    Args:
        scores (list): liste de scores
        cible (str): Le joueur dont on veut savoir son nombre d'apparition

    Returns:
        int: Nombre de fois que la cible est passé
    """
    cpt = 0
    for elem in joueurs:
        if elem == cible:
            cpt += 1
    return cpt

def test_top_players():
    """[summary]
    """
    assert top_players([], "") == 0
    assert top_players(['Batman', 'Robin', 'Batman', 'Joker', 'Batman'], "Batman") == 3
    assert top_players(['Batman', 'Robin', 'Batman', 'Joker', 'Batman'], "Joker") == 1
    assert top_players(['Batman', 'Robin', 'Batman', 'Joker', 'Batman'], "Bane") == 0


#Exercice 4.4
def position(joueurs,scores,cible):
    """Donne le positionnement d'un joueur

    Args:
        scores (list): Une liste de scores
        joueurs (list): [Une liste de joueurs
        cible (str): Le joueur ciblé
    Returns:
        int: Classement de la personne ciblé
    """
    if verif_tri(scores):
        for ind in range(len(joueurs)):
            if joueurs[ind] == cible:
                return ind + 1
    return None

def test_position():
    """[summary]
    """
    assert position([], [], "") is None
    assert position(['Batman', 'Robin', 'Batman', 'Joker', 'Batman'], [352100, 325410, 312785, 220199, 127853], "Batman") == 1
    assert position(['Batman', 'Robin', 'Batman', 'Joker', 'Batman'], [352100, 325410, 312785, 220199, 127853], "Joker") == 4
    assert position(['Batman', 'Robin', 'Batman', 'Joker', 'Batman'], [352100, 325410, 312785, 220199, 127853], "Poison Ivy") is None


#Exercice 4.5
def insertion(scores, ajout):
    """Permet d'insérer une valeur dans une liste sans détrier la liste

    Args:
        scores (list): liste de scores
        ajout (int): le score à ajouter

    Returns:
        list: liste avec l'ajout
    """
    if not ajout is None:
        if len(scores) != 0:
            for ind in range(len(scores)):
                if ajout > scores[ind]:
                    scores.insert(ind,ajout)
                    return scores
            else:
                scores.append(ajout)
        else:
            scores.append(ajout)
    return scores

def test_insertion():
    """[summary]
    """
    assert insertion([], None) == []
    assert insertion([352100, 325410, 312785, 220199, 127853], 392100) == [392100, 352100, 325410, 312785, 220199, 127853]
    assert insertion([352100, 325410, 312785, 220199, 127853], 332005) == [352100, 332005, 325410, 312785, 220199, 127853]
    assert insertion([352100, 325410, 312785, 220199, 127853], 127485) == [352100, 325410, 312785, 220199, 127853, 127485]
    assert insertion([], 500) == [500]

#Exercice 4.6

def insertion_nom(scores, ajout, auteur, joueurs):
    """Permet d'insérer une valeur dans une liste sans détrier la liste

    Args:
        scores (list): liste de scores
        ajout (int): le score à ajouter
        auteur (str): L'auteurs du score à ajouter
        joueurs (list): Liste de joueurs

    Returns:
        list: liste avec l'ajout
    """
    if not ajout is None:
        if len(joueurs) != 0:
            for ind in range(len(joueurs)):
                if ajout > scores[ind]:
                    joueurs.insert(ind,auteur)
                    return joueurs
            else:
                joueurs.append(auteur)
        else:
            joueurs.append(auteur)
    return joueurs

def test_insertion_nom():
    """[summary]
    """
    assert insertion_nom([], None, "", []) == []
    assert insertion_nom([352100, 325410, 312785, 220199, 127853], 365120, "Flash", ['Batman', 'Robin', 'Batman', 'Joker', 'Batman']) == ['Flash', 'Batman', 'Robin', 'Batman', 'Joker', 'Batman']
    assert insertion_nom([], 300000, "L'homme mystère", []) == ["L'homme mystère"]
    assert insertion_nom([352100, 325410, 312785, 220199, 127853], 334875, "Catwoman", ['Batman', 'Robin', 'Batman', 'Joker', 'Batman']) == ['Batman', 'Catwoman', 'Robin', 'Batman', 'Joker', 'Batman']


def insertion_totale(scores, val, auteur, joueurs):
    """Va ajouter une valeur et son auteurs aux liste donnée

    Args:
        scores (list): Liste de scores
        val (int): Le score à ajouter
        auteur (str): L'auteurs du score à ajouter
        joueurs (list): Liste de joueurs

    Returns:
        tuple: Les nouvelles listes joueurs et score
    """
    if verif_tri(scores):
        joueurs = insertion_nom(scores, val, joueurs, auteur)
        scores = insertion(scores, val)
        return (joueurs, scores)

def test_insertion_totale():
    """[summary]
    """
    assert insertion_totale ([], None, [], "") == ([], [])
    assert insertion_totale ([352100, 325410, 312785, 220199, 127853], 365120, ['Batman', 'Robin', 'Batman', 'Joker', 'Batman'], "Flash") == (['Flash', 'Batman', 'Robin', 'Batman', 'Joker', 'Batman'], [365120, 352100, 325410, 312785, 220199, 127853])
    assert insertion_totale ([], 300000, [], "L'Homme Mystère") == (["L'Homme Mystère"], [300000])


SCORES = [352100, 325410, 312785, 220199, 127853]
JOUEURS = ['Batman', 'Robin', 'Batman', 'Joker', 'Batman']
