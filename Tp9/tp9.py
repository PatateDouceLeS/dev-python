"""Init Dev : TP9"""


# ==========================
# Petites bêtes
# ==========================

def toutes_les_familles(pokedex):
    """détermine l'ensemble des familles représentées dans le pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        set: l'ensemble des familles représentées dans le pokedex
    """
    li_types = set()
    for (_, type) in pokedex:
        li_types.add(type)
    return li_types
#Complexité : O(N)

def nombre_pokemons(pokedex, famille):
    """calcule le nombre de pokemons d'une certaine famille dans un pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)
        famille (str): le nom de la famille concernée

    Returns:
        int: le nombre de pokemons d'une certaine famille dans un pokedex
    """
    cpt_type = 0
    for (_, type) in pokedex:
        if type == famille:
            cpt_type += 1
    return cpt_type
#Complexité : O(N)

def frequences_famille(pokedex):
    """Construit le dictionnaire de fréquences des familles d'un pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str)
        et la valeur associée est le nombre de représentants de la famille (int)
    """
    dico_freq =  {}
    for (_, type) in pokedex:
        if type in dico_freq:
            dico_freq[type] += 1
        else:
            dico_freq[type] = 1
    return dico_freq
#Complexité : O(N)

def dico_par_famille(pokedex):
    """Construit un dictionnaire dont les les clés sont le nom de familles (str)
    et la valeur associée est l'ensemble (set) des noms des pokemons de cette
    famille dans le pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str) et la valeur associée est
        l'ensemble (set) des noms des pokemons de cette famille dans le pokedex
    """
    dico_freq =  {}
    for (pokemon, type) in pokedex:
        if type not in dico_freq:
            dico_freq[type] = set()
        dico_freq[type].add(pokemon)
    return dico_freq
#Complexité : O(N)

def famille_la_plus_representee(pokedex):
    """détermine le nom de la famille la plus représentée dans le pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        str: le nom de la famille la plus représentée dans le pokedex
    """
    type_max = None
    nbr_pokemon_max = None
    dico_freq = frequences_famille(pokedex)
    for (type, nbr_pokemon) in dico_freq.items():
        if type_max == None or nbr_pokemon_max < nbr_pokemon:
            nbr_pokemon_max = nbr_pokemon
            type_max = type
    return type_max
#Complexité : O(N)

# ==========================
# La maison qui rend fou
# ==========================

def quel_guichet(mqrf, guichet_de_depart):
    """Détermine le nom du guichet qui délivre le formulaire A-38

    Args:
        mqrf (dict): représente une maison qui rend fou
        guichet (str): le nom du guichet de départ qui est le nom d'un guichet de la mqrf

    Returns:
        str: le nom du guichet qui finit par donner le formulaire A-38
    """
    gui_aller = guichet_de_depart
    while not mqrf[gui_aller] is None:
        gui_aller = mqrf[gui_aller]
    return gui_aller


def quel_guichet_v2(mqrf, guichet_de_depart):
    """Détermine le nom du guichet qui délivre le formulaire A-38
    ainsi que le nombre de guichets visités

    Args:
        mqrf (dict): représente une maison qui rend fou
        guichet (str): le nom du guichet de départ qui est le nom d'un guichet de la mqrf

    Returns:
        tuple: le nom du guichet qui finit par donner le formulaire A-38 et le nombre de
        guichets visités pour y parvenir
    """
    gui_aller = guichet_de_depart
    ind = 1
    while not mqrf[gui_aller] is None:
        gui_aller = mqrf[gui_aller]
        ind += 1
    return (gui_aller, ind)


def quel_guichet_v3(mqrf, guichet_de_depart):
    """Détermine le nom du guichet qui délivre le formulaire A-38
    ainsi que le nombre de guichets visités

    Args:
        mqrf (dict): représente une maison qui rend fou
        guichet (str): le nom du guichet de départ qui est le nom d'un guichet de la mqrf

    Returns:
        tuple: le nom du guichet qui finit par donner le formulaire A-38 et le nombre de
        guichets visités pour y parvenir
        S'il n'est pas possible d'obtenir le formulaire en partant du guichet de depart,
        cette fonction renvoie None
    """
    gui_aller = guichet_de_depart
    ind = 1
    while not mqrf[gui_aller] is None and ind <= len(mqrf):
        gui_aller = mqrf[gui_aller]
        ind += 1
    if ind <= len(mqrf):
        return (gui_aller, ind)
    return None


# ==========================
# Petites bêtes (la suite)
# ==========================


def toutes_les_familles_v2(pokedex):
    """détermine l'ensemble des familles représentées dans le pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        set: l'ensemble des familles représentées dans le pokedex
    """
    li_types = set()
    for (_, famille) in pokedex.items():
        for types in famille:
            li_types.add(types)
    return li_types

def nombre_pokemons_v2(pokedex, types):
    """calcule le nombre de pokemons d'une certaine famille dans un pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)
        types (str): Types de la famille concernée

    Returns:
        int: le nombre de pokemons d'une certaine famille dans un pokedex
    """
    cpt_type = 0
    for (_, famille) in pokedex.items():
        if types in famille:
            cpt_type += 1
    return cpt_type

def frequences_famille_v2(pokedex):
    """Construit le dictionnaire de fréqeunces des familles d'un pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str) et la valeur
        associée est le nombre de représentants de la famille (int)
    """
    dico_freq =  {}
    for (_, famille) in pokedex.items():
        for types in famille:
            if types in dico_freq:
                dico_freq[types] += 1
            else:
                dico_freq[types] = 1
    return dico_freq

def dico_par_famille_v2(pokedex):
    """Construit un dictionnaire dont les les clés sont le nom de familles (str)
    et la valeur associée est l'ensemble (set) des noms des pokemons de
    cette famille dans le pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str) et la valeur associée est
        l'ensemble (set) des noms des pokemons de cette famille dans le pokedex
    """
    dico_freq =  {}
    for (pokemon, famille) in pokedex.items():
        for types in famille:
            if types not in dico_freq:
                dico_freq[types] = set()
            dico_freq[types].add(pokemon)
    return dico_freq

def famille_la_plus_representee_v2(pokedex):
    """détermine le nom de la famille la plus représentée dans le pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        str: le nom de la famille la plus représentée dans le pokedex
    """
    type_max = None
    nbr_pokemon_max = None
    dico_freq = frequences_famille_v2(pokedex)
    for (types, nbr_pokemon) in dico_freq.items():
        if type_max == None or nbr_pokemon_max < nbr_pokemon:
            nbr_pokemon_max = nbr_pokemon
            type_max = types
    return type_max
