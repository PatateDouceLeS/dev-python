"""
Permet de modéliser un le_plateau de jeu avec :
    - une matrice qui contient des nombres entiers
    - chaque nombre entier correspond à un item :
      MUR, COULOIR, PERSONNAGE, FANTOME
"""
import matrice as m

MUR = 1
COULOIR = 0
PERSONNAGE = 2
FANTOME = 3

NORD = 'z'
OUEST = 'q'
SUD = 'w'
EST = 's'


def init(nom_fichier="./labyrinthe1.txt"):
    """Construit le plateau de jeu de la façon suivante :
        - crée une matrice à partir d'un fichier texte qui contient des COULOIR et MUR
        - met le PERSONNAGE en haut à gauche cad à la position (0, 0)
        - place un FANTOME en bas à droite
    Args:
        nom_fichier (str, optional): chemin vers un fichier csv qui contient COULOIR et MUR.
        Defaults to "./labyrinthe1.txt".

    Returns:
        le plateau de jeu avec les MUR, COULOIR, PERSONNAGE et FANTOME
    """
    plateau = m.charge_matrice(nom_fichier)
    nb_lignes = m.get_nb_lignes(plateau)
    nb_colonnes = m.get_nb_colonnes(plateau)
    m.set_val(plateau, 0, 0, PERSONNAGE)
    m.set_val(plateau, nb_lignes-1, nb_colonnes-1, FANTOME)
    return plateau


def est_sur_le_plateau(le_plateau, position):
    """Indique si la position est bien sur le plateau

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        [boolean]: True si la position est bien sur le plateau
    """
    return position in le_plateau


def get(le_plateau, position):
    """renvoie la valeur de la case qui se trouve à la position donnée

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple d'entiers de la forme (no_ligne, no_colonne)

    Returns:
        int: la valeur de la case qui se trouve à la position donnée ou
             None si la position n'est pas sur le plateau
    """
    if est_sur_le_plateau(le_plateau, position):
        return (m.get_val(le_plateau, position[0], position[1]))
    return None

def est_un_mur(le_plateau, position):
    """détermine s'il y a un mur à la poistion donnée

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple d'entiers de la forme (no_ligne, no_colonne)

    Returns:
        bool: True si la case à la position donnée est un MUR, False sinon
    """
    if est_sur_le_plateau(le_plateau, position):
        return get(le_plateau, position) == MUR
    return None
        


def contient_fantome(le_plateau, position):
    """Détermine s'il y a un fantôme à la position donnée

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        bool: True si la case à la position donnée est un FANTOME, False sinon
    """
    if est_sur_le_plateau(le_plateau, position):
        return get(le_plateau, position) == FANTOME
    return None

def est_la_sortie(le_plateau, position):
    """Détermine si la position donnée est la sortie
       cad la case en bas à droite du labyrinthe

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        bool: True si la case à la position donnée est la sortie, False sinon
    """
    if est_sur_le_plateau(le_plateau, position):
        return position == (m.get_nb_lignes(le_plateau)-1, m.get_nb_colonnes(le_plateau)-1)
    return None
    


def deplace_personnage(le_plateau, personnage, direction):
    """déplace le PERSONNAGE sur le plateau si le déplacement est valide
       Le personnage ne peut pas sortir du plateau ni traverser les murs
       Si le déplacement n'est pas valide, le personnage reste sur place

    Args:
        le_plateau (plateau): un plateau de jeu
        personnage (tuple): la position du personnage sur le plateau
        direction (str): la direction de déplacement SUD, EST, NORD, OUEST

    Returns:
        [tuple]: la nouvelle position du personnage
    """
    ancienne_pos = personnage
    (x, y) = personnage
    if direction == NORD:
        personnage = (x-1, y)
    elif direction == SUD:
        personnage = (x+1, y)
    elif direction == EST:
        personnage = (x, y+1)
    elif direction == OUEST:
        personnage = (x, y-1)
    if est_sur_le_plateau(le_plateau, personnage) and not est_un_mur(le_plateau, personnage):
        m.set_val(le_plateau, ancienne_pos[0], ancienne_pos[1], COULOIR)
        m.set_val(le_plateau, personnage[0], personnage[1], PERSONNAGE)
        return personnage
    personnage = ancienne_pos
    return personnage
        


def voisins(le_plateau, position):
    """Renvoie l'ensemble des positions cases voisines accessibles de la position renseignées
       Une case accessible est une case qui est sur le plateau et qui n'est pas un mur
    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        set: l'ensemble des positions des cases voisines accessibles
    """
    (x, y) = position
    voisins = set()
    voisins_potentiels = {(x-1, y), (x,y+1), (x+1,y), (x,y-1)}
    for position in voisins_potentiels:
        if est_sur_le_plateau(le_plateau, position) and not est_un_mur(le_plateau, position):
            voisins.add(position)
    return voisins




def fabrique_le_calque(le_plateau, position_depart):
    """fabrique le calque d'un labyrinthe en utilisation le principe de l'inondation :
       
    Args:
        le_plateau (plateau): un plateau de jeu
        position_depart (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        matrice: une matrice qui a la taille du plateau dont la case qui se trouve à la
       position_de_depart est à 0 les autres cases contiennent la longueur du
       plus court chemin pour y arriver (les murs et les cases innaccessibles sont à None)
    """
    calque = m.new_matrice(m.get_nb_lignes(le_plateau), m.get_nb_colonnes(le_plateau), None)
    m.set_val(calque, position_depart[0], position_depart[1], 0)
    marquage = True
    itération = 0
    while marquage:
        marquage = False
        for x in range(m.get_nb_lignes(calque)):
            for y in range(m.get_nb_colonnes(calque)):
                valeur_case = m.get_val(calque, x, y)
                if not est_un_mur(le_plateau, (x,y)) :
                    case_voisines = voisins(le_plateau, (x,y))
                    for case in case_voisines:  #Si les voisins sur le plateau sont bien sur le plateau et ne sont pas des murs
                        if m.get_val(calque, case[0], case[1]) is not None: #Si le voisin a une valeur
                            valeur_vois = m.get_val(calque, case[0], case[1]) + 1 
                            if valeur_case is None or valeur_vois < valeur_case: #Pour prendre la valeur du voisin la plus faible
                                m.set_val(calque, x, y, valeur_vois)
                                valeur_case = valeur_vois
                                itération = 0
            itération += 1
            if itération < 2: #Si on a fait 2 tours sans changer de valeurs
                marquage = True
    return calque



def ancien_fabrique_le_calque(le_plateau, position_depart):
    """fabrique le calque d'un labyrinthe en utilisation le principe de l'inondation :
       
    Args:
        le_plateau (plateau): un plateau de jeu
        position_depart (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        matrice: une matrice qui a la taille du plateau dont la case qui se trouve à la
       position_de_depart est à 0 les autres cases contiennent la longueur du
       plus court chemin pour y arriver (les murs et les cases innaccessibles sont à None)
    """
    calque = m.new_matrice(m.get_nb_lignes(le_plateau), m.get_nb_colonnes(le_plateau), None)
    m.set_val(calque, position_depart[0], position_depart[1], 0)
    marquage = True
    while marquage:
        ancien_calque = calque.copy()
        for x in range(m.get_nb_lignes(calque)):
            for y in range(m.get_nb_colonnes(calque)):
                valeur_case = m.get_val(calque, x, y)
                if not est_un_mur(le_plateau, (x,y)):
                    case_voisines = voisins(le_plateau, (x,y))
                    for case in case_voisines:
                        if est_sur_le_plateau(le_plateau, case) and not est_un_mur(le_plateau, case) and m.get_val(calque, case[0], case[1]) is not None:
                            valeur_vois = m.get_val(calque, case[0], case[1]) + 1
                            if valeur_case is None or valeur_vois < valeur_case:
                                m.set_val(calque, x, y, valeur_vois)
                                valeur_case = valeur_vois
        if ancien_calque == calque:
            marquage = False
    return calque


def fabrique_chemin(le_plateau, position_depart, position_arrivee):
    """Renvoie le plus court chemin entre position_depart position_arrivee

    Args:
        le_plateau (plateau): un plateau de jeu
        position_depart (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 
        position_arrivee (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        list: Une liste de positions entre position_arrivee et position_depart
        qui représente un plus court chemin entre les deux positions
    """
    fabriq_chemin = True
    plus_court = [position_arrivee]
    calque = fabrique_le_calque(le_plateau, position_depart)
    m.affiche(calque)
    position_actuelle = position_arrivee
    while fabriq_chemin:
        li_voisins = voisins(le_plateau, position_actuelle)
        val_pos_actuelle = m.get_val(calque, position_actuelle[0], position_actuelle[1])
        for (x, y) in li_voisins:
            val_voisin = m.get_val(calque, x, y)
            if val_voisin == val_pos_actuelle - 1:
                if (x, y) != position_depart:
                    plus_court.append((x, y))
                position_actuelle = (x, y)
        if position_actuelle == position_depart:
            fabriq_chemin = False
    return plus_court    


def deplace_fantome(le_plateau, fantome, personnage):
    """déplace le FANTOME sur le plateau vers le personnage en prenant le chemin le plus court

    Args:
        le_plateau (plateau): un plateau de jeu
        fantome (tuple): la position du fantome sur le plateau
        personnage (tuple): la position du personnage sur le plateau

    Returns:
        [tuple]: la nouvelle position du FANTOME
    """
    ancienne_pos = fantome
    chemin = fabrique_chemin(le_plateau, fantome, personnage)
    nvl_pos_fantome = chemin[-1]
    (pos_x, pos_y) = nvl_pos_fantome
    m.set_val(le_plateau, ancienne_pos[0], ancienne_pos[1], COULOIR) 
    m.set_val(le_plateau, pos_x, pos_y, FANTOME)
    return nvl_pos_fantome
