import unittest

def mystere_exo2(entree):
    """[summary]

    Args:
        entree ([type]): [description]

    Returns:
        [type]: [description]
    """
    xxx=0
    yyy=0
    # au début de chaque tour de boucle
    #  A COMPLETER
    for zzz in entree:
        if zzz%2==0:
            xxx+=1
        else:
            yyy+=1
    return xxx>=yyy

# exercice 3

def min_sup(liste_nombres,valeur):
    """trouve le plus petit nombre d'une liste supérieur à une certaine valeur

    Args:
        liste_nombres (list): la liste de nombres
        valeur (int ou float): la valeur limite du minimum recherché

    Returns:
        int ou float: le plus petit nombre de la liste supérieur à valeur
    """
    res=None
    # au début de chaque tour de boucle res est le plus petit élément déjà énuméré
    # supérieur à valeur
    for elem in liste_nombres:
        if elem > valeur:
            if res == None:
                res = elem
            if elem<res:
                res = elem
    return res



def test_min_sup():
    assert min_sup([8,12,7,3,9,2,1,4,9],5)==7
    assert min_sup([-2,-5,2,9.8,-8.1,7],0)==2
    assert min_sup([5,7,6,5,7,3],10)==None
    assert min_sup([],5)==None

# exercice 4
def nb_mots ( phrase ) :
    """Fonction qui compte le nombre de mots d'une phrase

    Args:
        phrase (str): une phrase dont les mots sont séparés par des espaces (éventuellement plusieurs)

    Returns:
        int: le nombre de mots de la phrase
    """    
    resultat =0
    c1 = ''
    # au début de chaque tour de boucle
    # c1 vaut le caractère qu'on a examiné avant
    # c2 vaut le caractère qu'on examine
    # resultat vaut le nombre de mots
    for c2 in phrase :
        if c1 == '' and c2 != ' ' :
            resultat += 1
        if c1 == ' ' and c2 != ' ':
            resultat += 1
        c1 = c2
    return resultat

def test_nb_mots():
    assert nb_mots("bonjour, il fait beau")==4
    assert nb_mots("houla!     je    mets beaucoup   d'  espaces    ")==6
    assert nb_mots(" ce  test ne  marche pas ")==5
    assert nb_mots("")==0 #celui ci non plus

# exercice 5.1
def Som_NbPairs (liste) :
    """Calcule la somme des nombres pairs de la liste

    Args:
        liste (list): liste de nombres entiers

    Returns:
        int: somme des pairs de la liste
    """
    Som = None
    for nombres in liste :
        if nombres%2 == 0 :
            if Som == None :
                Som = nombres
            else :
                Som += nombres
    return Som

def test_Som_NbPairs ():
    assert Som_NbPairs ([])==None
    assert Som_NbPairs ([1,3,4,5])==4
    assert Som_NbPairs ([1,3,5,7,9])==None
    assert Som_NbPairs ([2,24,8,9,7])==34

#exercice 5.2
def Der_Voy(mot) :
    """Renvoie la dernière voyelle d'un mot

    Args:
        mot (str): un mot 

    Returns:
        str: une voyelle
    """
    DerVoy=None
    for lettre in mot:
        if lettre in 'aeiouy':
            DerVoy = lettre
    return DerVoy

def test_Der_Voy ():
    assert Der_Voy("bonjour")=="u"
    assert Der_Voy("")==None
    assert Der_Voy("bhbhrvfvbrvfh")==None
    assert Der_Voy("uievfhvzdcjohvdjh")=="o"

#exercice 5.3
def Prop_Neg(liste):
    """Donne la proportion de nombres négatifs

    Args:
        liste (list): une liste de nombres entiers

    Returns:
        float: proportion de noombres négatifs
    """
    Tot_Nbr = 0
    NbrNeg = 0
    for nombres in liste:
        Tot_Nbr += 1
        if nombres < 0:
            NbrNeg += 1
    if Tot_Nbr != 0 :
        Prop = NbrNeg/Tot_Nbr 
    else :
        Prop = 0  
    return Prop

def test_Prop_Neg():
    assert Prop_Neg ([])==0
    assert Prop_Neg ([1,2,3,4])==0
    assert Prop_Neg ([4,-2,8,2,-2,-7])==0.5
    assert Prop_Neg ([4,-2,-7,-9,-2,0])==2/3

#exercice 6.1
def SommeNbr (Nbr):
    """Donne le somme de n premiers entiers 

    Args:
        Nbr (int): Nombres de la liste

    Returns:
        int: Somme de n nombres
    """
    Som=0
    for Nombres in range(Nbr):
        Som += Nombres
    return Som

def test_SommeNbr ():
    assert SommeNbr (0)==0
    assert SommeNbr (2)==1
    assert SommeNbr (3)==3
    assert SommeNbr (5)==10

#exercice 6.2
def Syracuse (Val_Init, Nbr):
    """Calcule le n-ième terme de la suite de Syracuse

    Args:
        Val_Init (int): Valeur de départ strictement positive de la suite
        Nbr (int): Nombres de valeurs de la suite

    Returns:
        int : Valeur finale de la suite
    """
    Val_inter = Val_Init
    for Nombre in range(Nbr):
        if Val_inter%2==0:
            Val_inter /= 2
        else:
            Val_inter = Val_inter*3+1
    Val_Finale = Val_inter
    if Val_Init < 0 :
        Val_Finale = None
    return Val_Finale

def test_Syracuse():
    assert Syracuse (2,4)==1
    assert Syracuse (4,3)==4
    assert Syracuse (-4,3)==None
    assert Syracuse (3,3)==16

#exercice 7 TD2 Ex3.1
def Syll(mot):
    """Compte le nombre de syllabe d'un mot

    Args:
        mot (str): Un mot quelconque

    Returns:
        int: Nbr de syllabes
    """
    NbrSyll = None
    Prec = ''
    for lettre in mot:
        Courant = lettre
        if NbrSyll == None:
            if Courant in 'aeiouy':
                NbrSyll = 1
        else:
            if Courant in 'aeiouy' and Prec not in 'aeiouy':
                NbrSyll += 1
        Prec = Courant
    return NbrSyll

def test_Syll():
    assert Syll ("ecouteur")==3
    assert Syll ("ici")==2
    assert Syll ("tableau")==2
    assert Syll ("bas")==1