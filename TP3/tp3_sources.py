# exercice 2
def ParitéNombres(ListeNbr):
    """Indique s'il y a plus de nombres pairs que d'impairs dans une liste de nombres

    Args:
        ListeNbr (liste): Une liste d'entiers relatifs

    Returns:
        bool: True s'il y a plus ou autant de chiffres pairs
    """
    Pairs=0
    Impairs=0
    # au début de chaque tour de boucle
    for Chiffre in ListeNbr:
        if Chiffre%2==0:
            Pairs+=1
        else:
            Impairs+=1
    return Pairs>=Impairs

def test_ParitéNombres () :
    assert ParitéNombres ([4,8,-2,-7,9,-11])
    assert not ParitéNombres ([-1,-5,7,81,4,-8])
    assert ParitéNombres ([74,7,6,25,-98,9])
    assert not ParitéNombres ([15,-1,71,-25,36,84])


# exercice 3
def min_sup(liste_nombres,valeur):
    """trouve le plus petit nombre d'une liste supérieur à une certaine valeur

    Args:
        liste_nombres (list): la liste de nombres
        valeur (int ou float): la valeur limite du minimum recherché

    Returns:
        int ou float: le plus petit nombre de la liste supérieur à valeur
    """
    res=None
    # au début de chaque tour de boucle res est le plus petit élément déjà énuméré
    # supérieur à valeur
    for elem in liste_nombres:
        if elem > valeur :
            if res == None or elem < res :
                res = elem
    return res

def test_min_sup():
    assert min_sup([8,12,7,3,9,2,1,4,9],5)==7
    assert min_sup([-2,-5,2,9.8,-8.1,7],0)==2
    assert min_sup([5,7,6,5,7,3],10)==None
    assert min_sup([],5)==None

# exercice 4
def nb_mots ( phrase ) :
    """Fonction qui compte le nombre de mots d'une phrase

    Args:
        phrase (str): une phrase dont les mots sont séparés par des espaces (éventuellement plusieurs)

    Returns:
        int: le nombre de mots de la phrase
    """    
    resultat =0
    Prec = ' '
    # au début de chaque tour de boucle
    # Prec vaut le caractère qu'on a examiné avant
    # Courant vaut le caractère qu'on examine
    # resultat vaut le nombre de mots
    for Courant in phrase :
        if Prec == ' ' and Courant != ' ':
            resultat += 1
        Prec = Courant
    return resultat

def test_nb_mots():
    assert nb_mots("bonjour, il fait beau")==4
    assert nb_mots("houla!     je    mets beaucoup   d'  espaces    ")==6
    assert nb_mots(" ce  test ne  marche pas ")==5
    assert nb_mots("")==0 #celui ci non plus

# exercice 5.1
def Som_NbPairs (liste) :
    """Calcule la somme des nombres pairs de la liste

    Args:
        liste (list): liste de nombres entiers

    Returns:
        int: somme des pairs de la liste
    """
    Som = None
    for nombres in liste :
        if nombres%2 == 0 :
            if Som == None :
                Som = nombres
            else :
                Som += nombres
    return Som

def test_Som_NbPairs ():
    assert Som_NbPairs ([])==None
    assert Som_NbPairs ([1,3,4,5])==4
    assert Som_NbPairs ([1,3,5,7,9])==None
    assert Som_NbPairs ([2,24,8,9,7])==34

#exercice 5.2
def Der_Voy(mot) :
    """Renvoie la dernière voyelle d'un mot

    Args:
        mot (str): un mot 

    Returns:
        str: une voyelle
    """
    DerVoy=None
    for lettre in mot:
        if lettre in 'aeiouy':
            DerVoy = lettre
    return DerVoy

def test_Der_Voy ():
    assert Der_Voy("bonjour")=="u"
    assert Der_Voy("")==None
    assert Der_Voy("bhbhrvfvbrvfh")==None
    assert Der_Voy("uievfhvzdcjohvdjh")=="o"

#exercice 5.3
def Prop_Neg(liste):
    """Donne la proportion de nombres négatifs

    Args:
        liste (list): une liste de nombres entiers

    Returns:
        float: proportion de noombres négatifs
    """
    Tot_Nbr = 0
    NbrNeg = 0
    for nombres in liste:
        Tot_Nbr += 1
        if nombres < 0:
            NbrNeg += 1
    if Tot_Nbr != 0 :
        Prop = NbrNeg/Tot_Nbr 
    else :
        Prop = 0  
    return Prop

def test_Prop_Neg():
    assert Prop_Neg ([])==0
    assert Prop_Neg ([1,2,3,4])==0
    assert Prop_Neg ([4,-2,8,2,-2,-7])==0.5
    assert Prop_Neg ([4,-2,-7,-9,-2,0])==2/3

#exercice 6.1
def SommeNbr (Nbr):
    """Donne le somme de n premiers entiers 

    Args:
        Nbr (int): Nombres de la liste

    Returns:
        int: Somme de n nombres
    """
    Som=0
    for Nombres in range(Nbr):
        Som += Nombres
    return Som

def test_SommeNbr ():
    assert SommeNbr (0)==0
    assert SommeNbr (2)==1
    assert SommeNbr (3)==3
    assert SommeNbr (5)==10

#exercice 6.2
def Syracuse (Val_Init, Nbr):
    """Calcule le n-ième terme de la suite de Syracuse

    Args:
        Val_Init (int): Valeur de départ strictement positive de la suite
        Nbr (int): Nombres de valeurs de la suite

    Returns:
        int : Valeur finale de la suite
    """
    Val_inter = Val_Init
    for Nombre in range(Nbr):
        if Val_inter%2==0:
            Val_inter /= 2
        else:
            Val_inter = Val_inter*3+1
    Val_Finale = Val_inter
    if Val_Init < 0 :
        Val_Finale = None
    return Val_Finale

def test_Syracuse():
    assert Syracuse (2,4)==1
    assert Syracuse (4,3)==4
    assert Syracuse (-4,3)==None
    assert Syracuse (3,3)==16

#exercice 7 Exo 1.1
def SomNbr (liste):
    """Calcule la somme d'une liste de nombres

    Args:
        liste (list): Liste de nombres à ajouter

    Returns:
        int: Somme des nombres
    """
    Som=None 
    for elem in liste:
        if Som == None :
            Som = elem
        else:
            Som += elem
    return Som

def test_SomNbr ():
    assert SomNbr ([])==None
    assert SomNbr ([4])==4
    assert SomNbr ([1,5,-7,6,-5])==0
    assert SomNbr ([4,5,8,9])==26

#exercice 7 Exo 1.2
def MaxNbr (liste):
    """Donne le maximum d'une liste

    Args:
        liste (list): Une liste de nombres entiers

    Returns:
        int: le nombre maximum de la liste
    """
    Max=None
    for elem in liste:
        if Max==None or elem>Max:
            Max=elem
    return Max

def test_MaxNbr ():
    assert MaxNbr ([])==None
    assert MaxNbr ([4,5,-7,-12])==5
    assert MaxNbr ([8,14,89,7])==89
    assert MaxNbr ([-1,1])==1

#exercice 7 Exo 1.3
def Occ_Mot(mot,LetOcc):
    """Donne l'occurence d'une lettre dans un mot

    Args:
        mot (str): Un mot
        lettre (str): La lettre qui va donner les occurences

    Returns:
        int: Nombre d'occurence de la lettre
    """
    NbrOcc = 0
    for lettre in mot:
        if lettre in LetOcc:
            NbrOcc += 1
    return NbrOcc


def test_OccMot():
    assert Occ_Mot('gruyère','r')==2
    assert Occ_Mot("chien",'u')==0
    assert Occ_Mot("","a")==0
    assert Occ_Mot("je suis une chèvre","e")==3

#exercice 7 Exo 2.1
def MinNbr (liste):
    """Donne le minimum d'une liste

    Args:
        liste (list): Une liste de nombres entiers

    Returns:
        int: le nombre minimum de la liste
    """
    Min=None
    for elem in liste:
        if Min==None or elem<Min:
            Min=elem
    return Min

def test_MinNbr ():
    assert MinNbr ([])==None
    assert MinNbr ([4,5,-7,-12])==-12
    assert MinNbr ([8,14,89,7])==7
    assert MinNbr ([-1,1])==-1

#exercice 7 Exo 2.2
def Ec_MinMax (liste):
    """Donne l'écart entre la maximum et le minimum d'une liste

    Args:
        liste (list): Liste de nombres

    Returns:
        int: Ecart entre le min et le max
    """
    Ecart = None
    Max=MaxNbr(liste)
    Min=MinNbr(liste)
    if Max or Min != None:
        Ecart = Max-Min
    return Ecart

def test_Ec_MinMax ():
    assert Ec_MinMax ([])==None
    assert Ec_MinMax ([4,5,-7,-12])==17
    assert Ec_MinMax ([8,14,89,7])==82
    assert Ec_MinMax ([-1,1])==2

#exercice 7 Exo 2.3
def Nbr_Sup10 (liste):
    """Donne le nombre de nombres supérieurs à 10

    Args:
        liste (list): Liste de nombres

    Returns:
        int: Total de nombre supérieur à 10
    """
    TotNbr = 0
    for elem in liste:
        if elem > 10:
            TotNbr += 1
    return TotNbr

def test_Nbr_Sup10 ():
    assert Nbr_Sup10 ([])==0
    assert Nbr_Sup10 ([1,4,5,-3])==0
    assert Nbr_Sup10 ([45,74,85,2])==3
    assert Nbr_Sup10 ([10,12,15,15])==3


#exercice 7 Exo 2.4
def MoyNeg (liste):
    """Donne la moyenne des nombres négatifs d'une liste

    Args:
        liste (list): Liste de nombres

    Returns:
        float: Moy des nombres
    """
    Nbr = 0
    TotNbrNeg = 0
    for elem in liste:
        if elem < 0:
            TotNbrNeg += elem
            Nbr += 1
    if TotNbrNeg == 0:
        Moy = None
    else:
        Moy = TotNbrNeg / Nbr
    return Moy

def test_MoyNeg ():
    assert MoyNeg ([])==None
    assert MoyNeg ([4,7,8,9])==None
    assert MoyNeg ([4,7,-9,8,-2])== -5.5
    assert MoyNeg ([-7,-9,-8,-9,-7])==-8

#exercice 7 TD2 Ex3.1
def Syll(mot):
    """Compte le nombre de syllabe d'un mot

    Args:
        mot (str): Un mot quelconque

    Returns:
        int: Nbr de syllabes
    """
    NbrSyll = None
    Prec = ''
    for lettre in mot:
        Courant = lettre
        if NbrSyll == None:
            if Courant in 'aeiouy':
                NbrSyll = 1
        else:
            if Courant in 'aeiouy' and Prec not in 'aeiouy':
                NbrSyll += 1
        Prec = Courant
    return NbrSyll

def test_Syll():
    assert Syll ("ecouteur")==3
    assert Syll ("ici")==2
    assert Syll ("tableau")==2
    assert Syll ("bas")==1

