from types import TracebackType


def pendu():
    mot_secret = input("Quel est votre mot ? ")
    mot_cour = []
    dico = dico_mot(mot_secret)
    mort = False
    trouver = False
    cpt_err = 0
    cpt_vic = 0
    for _ in mot_secret:
        mot_cour.append("_")
    while not mort and not trouver:
        print("Compteur erreurs : "+str(cpt_err)+"/11"))
        print(mot_cour)
        lettre = input("Donner une lettre ")
        if lettre in dico:
            for ind in dico[lettre]:
                mot_cour[ind] = lettre
                cpt_vic += 1
            if cpt_vic >= len(mot_secret):
                trouver = True
        else:
            print("Mauvaise lettre, tu pues")
            cpt_err += 1
            if cpt_err > 11:
                mort = True
    if mort:
        print("Oh le nul, il a perdu !")
    if trouver:
        print("gg wp, le mot était "+mot_secret)

def dico_mot(mot):
    dico = dict()
    for ind in range(len(mot)):
        if mot[ind] not in dico:
            dico[mot[ind]] = {ind}
        else:
            dico[mot[ind]].add(ind)
    return dico

pendu()
