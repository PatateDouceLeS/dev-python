TRAIN = ( " train " , 18)
NOUNOURS = ( " peluche " ,47)
VELO = ( " velo " , 24)
STYLO = ( " stylo " , 2)
CLAVIER = ( " clavier " , 25)
CONSOLE = ( " console " , 5)
TV = ( " tv " , 24)

CADEAUX = [TRAIN ,NOUNOURS ,VELO ,STYLO ,CLAVIER ,CONSOLE ,TV]

def somme(liste):
    som = 0
    for (_, poids) in liste:
        som += poids
    return som

def range_betement(liste_cadeaux):
    traineau = [[]]
    ind_malle = 0
    for (nom, poids) in liste_cadeaux:
        if somme(traineau[ind_malle]) + poids > 50:
            traineau.append([(nom, poids)])
            ind_malle += 1
        else:
            traineau[ind_malle].append((nom, poids))
    return traineau

def poids_cad(cadeau):
    return cadeau[1]

def range_intel(liste_cadeaux):
    liste_cadeaux = sorted(liste_cadeaux, key = poids_cad)
    traineau = range_betement(liste_cadeaux)
    return traineau

print("Voici le rangement bête :")
print(range_betement(CADEAUX))
print("Voici le rangement intelligent :")
print(range_intel(CADEAUX))