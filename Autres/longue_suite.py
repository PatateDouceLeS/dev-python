def longue_suite(liste):
    liste_triee = sorted(liste)
    liste_cour = []
    liste_max = []
    for nombre in liste_triee:
        if len(liste_cour) == 0:
            liste_cour.append(nombre)
        else:
            if nombre != liste_cour[-1]:
                if nombre == liste_cour[-1]+1:
                    liste_cour.append(nombre)
                else:
                    if len(liste_cour) > len(liste_max):
                        liste_max = liste_cour
                    liste_cour = [nombre]
    return liste_max
#Complexité O(NlogN)

LISTE1 = [19, 11, 5, 19, 15, 13, 11, 6, 21, 10, 19, 6, 12, 17, 20]
LISTE2 = [7, 1, 14, 11, 2, 17, 6, 15, 2, 8]
LISTE3 = [48, 11, 19, 36, 35, 28, 18, 45, 23, 6, 8, 14, 31, 3, 9, 2, 17, 48, 23,
45, 48, 11, 5, 18, 1, 8, 10, 42, 41, 10, 45, 31, 45, 10, 37, 3, 17, 15]
print(longue_suite(LISTE1))
print(longue_suite(LISTE2))
print(longue_suite(LISTE3))

def ajoute(nombre, liste_triee):
    ind = 0
    ajout = False
    while ind < len(liste_triee) and not ajout:
        if liste_triee[ind] > nombre:
            liste_triee.insert(ind, nombre)
            ajout = True
        ind += 1
    if not ajout:
        liste_triee.append(nombre)

LISTE_T1 = [3,4,5,7]
ajoute(2, LISTE_T1)
print(LISTE_T1)

LISTE_T1 = [3,4,5,7]
ajoute(8, LISTE_T1)
print(LISTE_T1)

LISTE_T1 = [3,4,5,7]
ajoute(4, LISTE_T1)
print(LISTE_T1)

LISTE_T1 = [3,4,5,7]
ajoute(7, LISTE_T1)
print(LISTE_T1)