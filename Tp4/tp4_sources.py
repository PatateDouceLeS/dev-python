"""
PERRIN Mathis
BUT Info Année 1
TP4
"""


# Exercice 1

def plus_long_plateau(chaine):
    """recherche la longueur du plus grand plateau d'une chaine

    Args:
        chaine (str): une chaine de caractères

    Returns:
        int: la longueur de la plus grande suite de lettres consécutives égales
    """
    lg_max = 0  # longueur du plus grand plateau déjà trouvé
    lg_actuelle = 0  # longueur du plateau actuel
    for ind in range(len(chaine)):
        if chaine[ind] == chaine[ind-1]:  # si la lettre actuelle est égale à la précédente
            lg_actuelle += 1
            if lg_actuelle > lg_max:
                lg_max = lg_actuelle
        else:  # si la lettre actuelle est différente de la précédente
            lg_actuelle = 1
    return lg_max


def test_plus_long_plateau():
    """[summary]
    """
    assert plus_long_plateau("") == 0
    assert plus_long_plateau("eeeee") == 5
    assert plus_long_plateau("maaanger") == 3
    assert plus_long_plateau("E") == 1


# Exercice 2

def ville_pop(liste_ville, population):
    """Donne la plus grande ville parmi une liste

    Args:
        liste_ville (list): Liste de villes françaises
        population (list): Liste de population de villes françaises

    Returns:
        str: La ville la plus grande
    """
    max_pop = 0
    grd_ville = None
    if len(liste_ville) == len(population):
        for ind in range(len(population)):
            if population[ind] > max_pop:
                grd_ville = liste_ville[ind]
                max_pop = population[ind]
    return grd_ville


def test_ville_pop():
    """[summary]
    """
    assert ville_pop(["Blois", "Bourges", "Chartres", "Châteauroux", "Dreux", "Joué-lès-Tours", "Olivet", "Orléans", "Tours", "Vierzon"], [45871, 64668, 38426, 43442, 30664, 38250, 22168, 116238, 136463, 25725]) == "Tours"
    assert ville_pop([], []) == None
    assert ville_pop(["Château-Renault", "Tours"], [5000, 136463]) == "Tours"
    assert ville_pop(["Nantes", "Saint-Nazaire"], [303382]) == None

# Exercice 3


def transfo(chaine):
    """Transforme une chaine de caratère en entier

    Args:
        chaine (str): chaine de nombres à transformer

    Returns:
        int: chaine transformée en entier
    """
    res = 0
    if len(chaine) != 0:
        for ind in range(len(chaine)):
            if chaine[ind] == "1":
                res += 1*10**(len(chaine)-(1+ind))
            else:
                if chaine[ind] == "2":
                    res += 2*10**(len(chaine)-(1+ind))
                else:
                    if chaine[ind] == "3":
                        res += 3*10**(len(chaine)-(1+ind))
                    else:
                        if chaine[ind] == "4":
                            res += 4*10**(len(chaine)-(1+ind))
                        else:
                            if chaine[ind] == "5":
                                res += 5*10**(len(chaine)-(1+ind))
                            else:
                                if chaine[ind] == "6":
                                    res += 6*10**(len(chaine)-(1+ind))
                                else:
                                    if chaine[ind] == "7":
                                        res += 7*10**(len(chaine)-(1+ind))
                                    else:
                                        if chaine[ind] == "8":
                                            res += 8*10**(len(chaine)-(1+ind))
                                        else:
                                            if chaine[ind] == "9":
                                                res += 9 * \
                                                    10**(len(chaine)-(1+ind))
    else:
        res = None
    return res


def test_transfo():
    """[summary]
    """
    assert transfo("") == None
    assert transfo("458") == 458
    assert transfo("12") == 12
    assert transfo("8052") == 8052


# Exercise 4
def recherch_mots(liste, lettre):
    """Recherche les mots commençant par une certaine lettre

    Args:
        liste (list): Liste de mots
        lettre (str): Une lettre quelconque

    Returns:
        liste: Liste de mots commançant par la lettre donné
    """
    res = []
    for elem in liste:
        if elem != '':
            if elem[0] == lettre:
                res.append(elem)
    return res


def test_recherch_mots():
    """[summary]
    """
    assert recherch_mots([], "w") == []
    assert recherch_mots(["", "a", "bb"], "a") == ["a"]
    assert recherch_mots(["chevre", "vache"], "r") == []
    assert recherch_mots(["hvzvzgv", "héros", "chien", "histoire"], "h") == [
        "hvzvzgv", "héros", "histoire"]

# Exercise 5


def nbr_mots(phrase):
    """Donne le nombres de mots d'une phrase

    Args:
        phrase (str): Une phrase quelconque

    Returns:
        int: Nombres de mots que contient la phrase
    """
    mots = []
    mot_cour = ''
    for courant in phrase:
        if courant.isalpha():
            mot_cour += courant
        else:
            if mot_cour != '':
                mots.append(mot_cour)
                mot_cour = ''
    if phrase != "":
        if phrase[len(phrase)-1].isalpha():
            mots.append(mot_cour)
    return mots

def test_nbr_mots():
    """[summary]
    """
    assert nbr_mots("") == []
    assert nbr_mots("hey") == ["hey"]
    assert nbr_mots(" ceci est un test") == ["ceci", "est", "un", "test"]
    assert nbr_mots("les mains vel sont incroyables !") == [
        "les", "mains", "vel", "sont", "incroyables"]


# Exercise 6
def rechech_nbr_mot(phrase, lettre):
    """Met dans une liste les mots d'une phrase commençant par une certaine lettre

    Args:
        phrase (str): Une phrase quelconque
        lettre (str): Une lettre quelconque

    Returns:
        list: Liste de mots
    """
    res_int = nbr_mots(phrase)
    res = recherch_mots(res_int, lettre)
    return res


def test_rechech_nbr_mot():
    """[summary]
    """
    assert rechech_nbr_mot("", "a") == []
    assert rechech_nbr_mot("bonjour les amis", "a") == ["amis"]
    assert rechech_nbr_mot("essayez python, c'est sympa!", "a") == []
    assert rechech_nbr_mot("bonjour je m'appelle mathis", "") == []

# Exercise 7


def liste_bool(entier):
    """Fais une liste composé de True et False

    Args:
        entier (int): Un entier quelconque

    Returns:
        list : Liste de booléens
    """
    booleens = []
    nbr_false = 0
    for i in range(entier+1):
        if nbr_false <= 1:
            booleens.append(False)
            nbr_false += 1
        else:
            booleens.append(True)
    return booleens


def test_liste_bool():
    assert liste_bool(4) == [False, False, True, True, True]
    assert liste_bool(3) == [False, False, True, True]
    assert liste_bool(0) == [False]
    assert liste_bool(1) == [False, False]


def crible_base(liste, nbr):
    """Met en false tout les multiple d'un nombre

    Args:
        entier (int): Taille de la liste de bool créée
        nbr (int): Nombre de base des multiples à suppr

    Returns:
        liste: liste de booleens partiellement criblé
    """
    for ind in range(len(liste)):
        if nbr > 1:
            if ind%nbr == 0 and ind != nbr:
                liste[ind] = False
    return liste

def test_crible_base():
    """[summary]
    """
    assert crible_base([False], 3) == [False]
    assert crible_base([], 3) == []
    assert crible_base([False, False, True], 1) == [False, False, True]
    assert crible_base([False, False, True, True, True, True, True, True, True, True, True], 2) == [False, False, True, True, False, True, False, True, False, True, False]

def erath(entier):
    """Effectue le crible d'Erathostène

    Args:
        entier (int): longueur de la liste de base

    Returns:
        list: liste de nombre premier
    """
    liste_int = liste_bool(entier)
    for elem in range(entier):
        liste_int = crible_base(liste_int, elem)
    return liste_int

def test_erath():
    """[summary]
    """
    assert erath(0) == [False]
    assert erath(2) == [False, False, True]
    assert erath(8) == [False, False, True, True, False, True, False, True, False]
    assert erath(12) == [False, False, True, True, False, True, False, True, False, False, False, True, False]

